---
title: Markus le Korrigan
date: Décembre 2019
ordre: 3
summary: Markus le korrigan avait toujours vécu dans cette chaumière. Comme son père avant lui et comme son grand-père encore avant, il gardait la famille qui l'habitait...
---
Markus le korrigan avait toujours vécu dans cette chaumière. Comme son père avant lui et comme son grand-père encore avant, il gardait la famille qui l'habitait. Depuis des lustres, il s'arrangeait pour que le feu dans l'âtre soit nourri pendant la nuit, que les rats et souris se tiennent loin du garde manger et que rien ne brûle dans le four. Il était une sorte d'ange gardien minuscule et invisible, s'agitant jour et nuit pour bricoler ce qui menaçait de se casser, remplir les pots qui se vidaient et faire disparaître les crasses et la poussière.

Même si personne dans la maisonnée n'était dupe, même si tout le monde savait qu'un petit être vivait dans leurs placards, on préférait l'ignorer. On préférait faire semblant de ne pas voir ses petits miracles ni les discrètes traces de son passage. Ne voyez cependant pas ici un acte hautain du grand peuple vers le petit. Non, on appréciait la présence de Markus, elle était même devenue indispensable, mais on l'ignorait par tradition.

Car c'est ainsi qu'il fallait faire, avait un jour dit Ronan à sa fille Hélène, comme son père le lui avait dit bien des années auparavant. Les korrigans étaient les créatures les plus douces, mais aussi les plus timides que l'on pouvait voir dans ses contrés, à vouloir les apercevoir ou leur parler, on risquait de les effrayer, de les faire fuir à tout jamais.

Alors, des grands-parents aux petits enfants en passant par les oncles et les tantes, jamais on évoquait cette présence bien aimable. À l'occasion, on accusait même les chats d'emmêler le pelotes de laines, quand Markus y avait fait une sieste impromptue.

Cependant, les jours de fêtes, on "oubliait" un morceau de galette ou un bout de biscuit près du placard. Yohann, le grand-père, avait pris cette curieuse habitude, tous les printemps, au moment de déboucher la première bouteille du nouveau cru, d'en laisser un fond de verre sur la table avant d'aller se coucher. Ce soir-la, en éteignant la dernière chandelle, l'ancêtre grognait dans sa barbe.

— N'hésite pas mon gars, ça te rendra grand et fort et puis ça te protégera du froid !

Le lendemain, on retrouvait toujours le verre vide renversé après une maladresse, la cendre froide dans l'âtre et la grande poêle de cuivre tombé par terre. On raconte même que les années ou le vin était fort, on entendait un léger ronflement s'échapper du placard toute la matinée suivante. Ce jour de fête avinée était bien le seul jour de l'année ou le korrigan manquait à ses devoirs.

Parfois, lors des longues veillées d'hivers, on entendait un discret chant, presque imperceptible reprendre les airs de flûtes joués par l'aîné de la famille. D'autres fois, quand l'oncle Harold revenait en ces terres après des mois de voyages de par le monde, on voyait la porte du placard s'entre-ouvrir, comme si son occupant voulait lui aussi profiter des histoires d'aventure du truculent voyageur.

Mais à part cela, rien ou presque rien ne trahissait la présence de Markus.

Les jours, les semaines, les mois, les saisons et enfin les années passèrent. Bientôt Markus n'eut plus à effacer les taches d'encres sur les cahiers des enfants, il tressa des couronnes de fleurs pour le mariage de la jeune Hélène, laissa échapper une larme pour le départ du joueur de flûte, pleura à chaude larme la mort du grand-père et eut le cœur serré en constatant que l'oncle Harold ne revenait plus narrer ses voyages. Puis il recommença à effacer les tâches d'encre sur les cahiers de nouveaux enfants.

Le temps semblait couler sur la petite famille qui, durant toutes ces années, jouissait de la bonne fortune que leur amenait la présence féerique du Korrigan. Toujours la maladie se tenait loin des bêtes, toujours les récoltes étaient bonnes, les potagers et vergers garnis et jamais le lait ou le vin ne tournait.

Hélas, le temps n'épargnait pas le monde alentour. La moquerie, la crainte, voir la haine du petit peuple envahissaient les esprits des maisons voisines. Et alors que tout le monde se tournait vers les lumières de la ville, on tournait le dos à celle des vieilles histoires. On ne se réveillait plus avec le soleil, mais avec les heures d'ouverture des usines et des bureaux. On ne disposait plus de pierre sur les pas des maisons pour souhaiter la bienvenue, mais on avait pris l'habitude de s'enfermer chez soi à double tours à la nuit tombée. On n'entendait presque plus les éclats de voix, les contes et la musique des veillées dans les chaumières. Au yeux de la famille, les autres, ceux qui n'avaient la chance d'avoir un korrigan pour enchanter leur quotidien, étaient devenus tristes et cyniques. Aux yeux du monde, les légendes de fées et de korrigans, la magie des druides et des sirènes n'étaient plus que des superstitions imbéciles, des histoires de croquemitaines pour effrayer les sales gosses et les vieillards à moitié fous. L'air du temps était à l'efficacité, au pragmatisme et au rationnel.

Et puis, malgré toute la résistance de la famille, tous les efforts de Markus, ce qui devait arriver arriva. La famille dût vendre la chaumière loin des routes pour s'installer en ville, là où étaient les promesses de travail, d'argent et de ce nouveau confort moderne.

Durant la dernière soirée dans la chaumière, il n'eut pas de veillée. Le notaire été venu le matin même faire signer le contrat de vente, les bagages étaient bouclés dans des malles noires, les derniers fruits et légumes du jardin étaient en bocaux, les animaux qui n'avaient pas été vendus à la foire étaient remis en liberté. Hélène, devenue mère de famille ferma les volets et tira les rideaux en sachant qu'elle n'aurait pas à les ouvrir le lendemain.

— Et Markus, il vient avec nous ? Demanda le fils cadet, un garçonnet d'à peine six ans à sa mère.

— Hélas non, répondit-elle le cœur gros. Markus est le gardien de la maison, il ne la quittera pas. Il va la tenir en attendant que quelqu'un la rachète. Ce sera sans doute un riche marchand qui viendra y passer ses vacances.

Markus manqua de s'étouffer en entendant la réponse.

Bien sûr que si, il allait venir. Son baluchon était déjà prêt, sa besace était pleine de morceaux de pain pour la route et il avait même pensé à voler un morceau d'étoffe qui ferait un hamac de voyage tout à fait acceptable. La situation lui pesait aussi, lui non plus n'avait pas envie de quitter cette modeste ferme où il avait vécu toute sa vie. Mais en bon gardien, de la famille et non des lieux, il se devait de faire contre mauvaise fortune bon cœur, il devait être l'étincelle d'espoir pour cette famille perdue dans la tristesse.

Comme il n'y avait plus de jardin, plus de chat ni de chien, plus de meuble et bientôt plus de maison, Markus décida d'en finir avec la vieille coutume pour donner du baume au cœur. Il brisa ses habitudes de lutin invisible et se montra.

— Hélène, je viens avec vous, vous m'emmenez en ville dans vos bagages que ça vous plaise ou non ! Hurl- t-il aussi fort que lui permettaient ses minuscules poumons.

Il s'attendait aux hourras, aux cris de surprise de la petite communauté, il s'apprêtait à tenir de longues discussions avec la famille. Il voulait dire et expliquer ce que ses ancêtres avaient toujours tenu secret, il voulait philosopher avec les adultes et amuser les enfants. Il voulait faire encore plus partie de cette famille.

Hélas rien ne vient à part un sanglot étouffé du jeune garçon.

— Hé ! Je viens ! Répéta Markus en tapant des pieds et agitant les bras, comme on le ferait pour attirer un bateau au loin.

Il n'eut toujours aucune réaction et le garçon retourna tristement fermer son petit bagage.

— Hé vous m'entendez ? S'époumona le Korrigan en posant ses mains autour de sa bouche en porte voix.

Mais il fallait se rendre à l'évidence, non, on ne l'entendait pas.

« Et bien ils sont tous devenus sourds », pensa Markus en claquant des doigts, utilisant ses pouvoirs féeriques pour ouvrir une des malles de voyage. Au grand maux, les grands remèdes, il allait faire plus de bruit qu'aucun korrigan n'avait fait avant lui.

En voyant les bagages s'ouvrir magiquement, le regard de la mère s'illumina.

"Markus" souffla-t-elle en s'approchant un sourire naissant aux lèvres. Un vacarme de tous les diables retentit alors. Le Korrigan, armé d'une cuillère en bois, tambourina de toutes ses forces sur les poêles et casseroles empaquetées. Toute la famille se retourna enfin.

— Markus, c'est Markus ! Hurla un enfant voulant enfin voir ce drôle de personnage qu'il espérait surprendre depuis sa plus petite enfance.

Mais la joie fût de bien courte durée.

Markus était tout simplement invisible aux yeux de la famille, qui ne pouvait voir qu'une cuillère en bois flottant comme par magie battant frénétiquement contre le fer.

— C'est normal qu'on ne pouvait pas le voir, Maman, s'amusa un enfant, il est invisible.

— Invisible ! Manqua s'étouffer une seconde fois le korrigan. Bien sûr que non vous pouvez me voir

— Non mon chat repris la mère, on doit le voir et l'entendre.

— Alors, pourquoi je ne ...

— Hé Ho ! beugla Markus, Je suis là !

Invisible, inaudible, Markus ne pouvait plus communiquer avec les membres de sa propre famille qu'en cognant les ustensiles de cuisines.

— Il veut nous parler, compris Hélène, mais il ne peut simplement plus, expliqua-t-elle les larmes aux yeux.

— Mais pourquoi ?

— Je crois qu'en choisissant de partir, nous avons quitté le monde de la féerie. Jamais plus nous ne pourrons voir les lutins, les fées et les sirènes, ils ne font plus partie de nos vies.

Les enfants éclatèrent en sanglot. Markus laissa échapper sa frustration dans un dernier coup de poing rageur contre la casserole et un vieux juron en elfique.

Mais il est bien connu que les korrigans ne connaissent pas l'abattement, ils ne connaissent pas l'abandon.

Alors, il allait partir, oui, sur-le-champs, il chargea son bagage sur son dos et décida de retourner en féerie demander des explications. Et on allait l'entendre !

Il puisa dans sa mémoire pour se souvenir de la cérémonie d'ouverture du passage vers le monde des elfes. Son père le lui avait un jour enseigné, mais il n'avait pas été très attentif. Qui aurait cru qu'il voudrait un jour quitter le monde des humains ?

Il lui semblait se souvenir qu'il fallait utiliser une brindille de sureau, huit pièces sorties d'un ruisseau, une plume de corbeau et une formule magique. Il eut beau disposer les objets de mille-et-une façon, varier l'élocution et l'accent qu'il prenait pour réciter l'incantation, il n'arrivait pas ouvrir le passage.

Il était là, bloqué dans un monde qui lui échappait, il savait que la solution était toute proche, que seul sa mémoire défaillante l'empêchait d'agir. Il se maudit de toutes les façons possibles.

Jusqu'à la tombée de le nuit, il tourna, disposa, cassa, cueillit, chanta, mais rien n'y fit.

— Mais ouvrez-moi, bon sang de mouette ! Finit-il par lâcher en s'adossant essoufflé contre une souche.

Et comme s'il suffisait de demander, une douce mélodie se fit entendre, une chaleur rassurante envahit les lieux et un halo de lumière dorée apparut devant ses yeux. Sans perdre une seconde, Markus plongea à pied joint dans le portail. Il partait explorer un monde qui était le sien mais qu'il n'avait jamais connu.

Il atterrit au pays des Korrigans, une fabuleuse contrée où tout était minuscule, à la taille de ses habitants. Là-bas tout était joyeux et mystérieusement bruyant pour un peuple aussi discret dans le monde des humains.

Il y avait dans ce village une fête une nuit sur deux, un bal était organisé les autres jours. Du levers de la lune jusqu'à son coucher on mangeait, buvait, dansait et jouait de la musique. Au début du jour chacun rentrait chez soi pour dormir, pour reposer les jambes trop sollicitées par les longues danses et les estomacs victimes des excès de chaire et de boisson. Puis le village reprenait vie quand le soleil était à son zénith et chacun vaquait à ses occupations jusqu'au crépuscule. Occupation qui souvent, étaient de participer à la préparation de la fête suivante.

Malgré cette terre pleine de promesses, Markus ne s'y attarda pas. Il prit juste le temps de demander son chemin et de décliner les invitations de ses congénères au bal du soir.

— Je suis désolé, mais j'ai à faire, c'est très grave ! Répétait Markus, à tous les korrigans qu'il croisait et qui, inévitablement, l'invitaient. Je dois voir la reine des elfes au plus vite.

On n'en tenu pas rigueur à Markus

— C'est l'apanage des jeunes de courir, dit un vieux korrigan à la barbe si longue qu'elle était plus grande que le reste de son corps.

— Et puis il vient de chez les humains, rajouta une autre, ces grands dadais passent leurs vies pressés, ils ont dû le contaminer.

Le passage de ce drôle de personnage, de ce jeune korrigan d'à peine quatre-vingt-cinq ans, autant dire un enfant, seul, courant en disant avoir une mission était un spectacle bien étonnant pour la population si insouciante de ce pays.

On lui prêta un bâton de marche, remplit sa besace de petits fours pour la route et lui fit promettre qu'il s'arrêterait au retour. Puis on le regarda partir en trinquant à son succès.

Puis Markus se remit en route.

Toutes les merveilles du monde des elfes lui passaient devant les yeux sans qu'il ne s'en étonna ni même y prêta la moindre attention.

Quand il traversa le champ de fleurs tenu par le clan des fées coquelicots, le souvenir de l'odeur de pain chaud sortant du four était plus fort que les arômes subtiles de roses. Quand il gravit les montagnes de l'harmonie, la symphonie des lutins joueurs de violon lui semblait bien fade devant les notes de flûtes des veillées. Peu importait la splendeur des paysages qu'il traversait, les forêts d'émeraude, les rivages de la rivière d'ivoire ou le ciel rose au-dessus des plages d'or. Lui ne voulait voir que les plaines trempées par la pluie et battues par les vents. Et une chaumière perdue dans les collines.

Après des jours de marche, alors qu'il avait dû réparer ses souliers au moins deux fois et écoulé depuis longtemps ses rations humaines et dons des korrigans. Il arriva enfin aux abords de la forêt originelle. Cette forêt où était née tous les peuples féeriques, mais que tous avaient choisi de quitter. Cette forêt qui était le dernier endroit sauvage de monde de la féerie, le dernier endroit où la musique, la poésie et la joie de vivre n'avait pas encore été adoptées comme philosophie.

Cette forêt surtout où vivait la reine des elfes, la dernière à ne pas l'avoir déserté.

Sous les ramures entrecroisées des arbres tordus par le temps, il semblait faire constamment nuit. Il fallait marcher prudemment sur un sol parcouru de racines noueuses. Souvent, il fallait se frayer un passage dans les haies de ronces et d'aubépines. Le chant des merles et roitelets qui avait accompagné le korrigan durant tout son périple avait été remplacé par le coassement des corbeaux et le hululement des hiboux et des chouettes.

Alors qu'après un faux pas, Markus se retrouva suspendu par son gilet à un arbre épineux, il se demanda si ce voyage n'était pas voué à l'échec. Arriverait-il à sortir de ce bois ? Trouverait-il la reine des Elfes ? Et si oui, sera-t-il bien accueillis. Lui qui avait imaginé une reine à la douceur inimaginable, une reine à l'image de son ses sujets, se ravisa. Il fallait être un personnage à moitié et fou et assoiffé de sang pour vivre ici depuis des millénaires.

Quand enfin, après s'être débattu pendant de longues minutes, il arriva à toucher le sol. Il s'imagina faire machine à arrière, revenir au pays des korrigans pour y apprendre l'art des claquettes de ses congénères et leur apprendre en retours les danses traditionnelles de son pays humain.

L'image fugace de la famille en pleur, suivie par celle des moments de joie passés avec eux le ramena à la raison. Après tout ce qu'il avait fait, il ne pouvait pas faire demi-tour. Pas maintenant. Il était de toute façon perdu dans cette satané forêt, il n'était pas sûr de pouvoir en ressortir. Alors, autant se démener pour trouver la seule guide qui y vivait encore.

Malgré sa petite taille, ses tours de magie bien pratique et son agilité passant pour exceptionnelle chez les humains, le korrigan avait de plus en plus de mal à progresser dans le dédale végétal. C'était donc un Korrigan en haillon, écorché et épuisé qui fut interpellé par une voix si profonde qu'elle semblait sortir des tréfonds de la terre.

— Je ne pensais pas te voir aussi tôt jeune korrigan, bienvenue dans mon humble demeure.

Il avait réussi, il avait trouvé la reine des elfes, ce n'était pas le moment de tout gâcher.

En s'inclinant aussi bas qu'il le put, Markus récita ses hommages.

— Je remercie votre splendeur et lui demande de m'excuser de troubler sa paix.

— Je n'en suis en rien courroucé, pour dire vrai j'attendais avec impatience ton arrivée.

Dans un bruissement, une palissade de branches griffues et de plantes effilées s'ouvrit pour laisser passer sa majesté reine des elfes.

Comme Markus l'avait imaginé, elle était la plus belle des elfes. Son visage était fin et ses yeux d'un gris semblable à la lune. Ses lèvres d'un rouge éclatant tranchait avec le rose délicat de son teint. Ses mouvements étaient amples et gracieux. Ses magnifiques atours, une robe verte et blanche et ses bijoux fait de perle de bois, de feuilles séchées et de plumes aurait été détruit si elle ne commandait pas aux arbres de lui laisser un passage.

— Vous m'attendiez ? Demanda t-il en s'adossant à une racine, oubliant, fidèle à son peuple, les bonnes manières en vigueur chez les elfes.

La Reine ne lui tenu pas compte de son comportement.

— Et bien les nouvelles vont plus vite que les korrigans, depuis que j'ai appris que tu me cherchais, je suis curieuse de savoir ce que tu veux. Les visiteurs de l'autre monde sont rares. Et tu as fait particulièrement vite. Expliqua la reine en s'asseyant à son tour pour se rapprocher de la taille de son interlocuteur.

— J'ai marché tous les jours sans m'arrêter !

— Cela doit être rudement important, alors je t'écoute, Irikis le Korrigan, qui puis-je pour toi ?

Markus tiqua au nom Irikis, puis il se souvient que Markus, le nom qu'il avait toujours porté lui avait été donné par les humains. Il n'avait jamais pensé qu'il pouvait avoir un autre nom dans son monde d'origine. Il oublia cela pour se concentrer sur son effet d'annonce.

— Alors, voilà, je m'appelle Markus et d'aussi loin que je me souvienne, j'ai vécu avec et pour une famille dans le monde des humains. Tout allait pour le mieux et cela aurait pu durer pour siècles encore s'ils n'avaient pas choisi de quitter la chaumière.

La reine esquissait déjà un sourire un triste, signe qu'elle avait compris le nœud du problème. Markus, qui tout ce temps s'était battu, avait étouffé le chagrin dans un voile de volonté farouche, se laissa pour la première fois gagner par l'émotion. Ses yeux s'alourdissaient de larmes alors qu'il continua son récit.

— Puis un jour, ils ont dû partir, laisser leur bonheur, les animaux et l'odeur du pain qui sort du four à bois pour aller en ville. Et là, j'ai compris que j'étais devenu invisible, qu'il ne me voyait plus ! Moi qui n'ai toujours vécu que pour eux ! Je ne pourrai plus jamais faire partie de leur vie, jamais !

La colère et le désespoir mêlés distordaient de plus en plus la voix du Korrigan, si bien que même la reine des elfes en personne eut de mal à ramener la quiétude en lui.

Alors, en pesant chacun de ses mots, en se tordant encore un peu vers le sol pour se rapprocher de Markus, elle commença.

- C'est hélas ce que je craignais, Markus. Le monde des elfes et celui hommes, qui autrefois ne faisaient qu'un est un seul royaume, se séparent de plus en plus vite. Il été écrit que la frontière entre nous deviendrai infranchissable. Ce n'est plus qu'une question de lune avant que la féerie soit complètement chassée de la terre. Et ni toi, ni moi, ni même les plus sages des Hommes ne peuvent rien y faire.

Markus éclata en sanglot. Il ne pourrait jamais retourner chez lui. La reine elle-même avait fait aveux de faiblesse, plus de chaumière, plus d'espoir, plus rien.

A moins que ...

La reine reprit.

— Il te reste une chance, mais qui aura un goût amer ...

Il se redressa.

— Mais tu dois en payer le prix. Plus de magie, plus aucun retour dans ton monde possible. Tu ne pourras jamais plus communier avec tes ancêtres ni avoir de descendance ...

Contre toutes les convenances, Markus coupa la parole de son monarque.

— C'est d'accord, quel-qu'en soit le prix, il n'y a rien que je puisse sacrifier qui a plus de valeur que ma famille. N'essayez pas de me dissuader.

La reine qui connaissait le caractère buté du petit peuple n'essaya même pas.

Sa voix qui semblait n'être que de miel se durcit brusquement. Elle gronda comme le tonnerre dans la vallée.

— Irkis le Korrigan, je te déclare ennemi de mon peuple. Des demeures des nains sous la terre aux nuages de soie, tu n'es plus le bienvenu sur mon domaine. Par les pouvoirs qui m'ont été confiés par nos ancêtres, Moi Reine des elfes et fille des forêts, je te bannis !

La sanction était aussi immédiate qu'irrévocable, un son strident, une vague de froids et une fumée noire s'abattit sur Markus. Il disparut soudainement, dans un craquement sinistre pareil à celui du bois mort que l'on casse.

Mais il n'y avait nulle once de haine ou de colère dans la voie de la reine, à peine un sursaut d'amertume. Elle savait qu'en bannissant ce curieux Korrigan, elle avait brisé son lien avec la féerie, à tout jamais. Alors, au moment où la frontière des mondes allait se déchirer, il ne serait pas rappelé.

Il était désormais sans royaume, mais libre de vivre où il le pouvait, où on voudrais bien de lui.

Il était sans royaume, mais heureux.

Il atterrit lourdement, à l'endroit même il avait sauté dans le portail. Devant les restes des plumes, pièce et brindilles disposées.

Encore endolorie par l'expérience de son bannissement, les idées encore brumeuses après un passage d'un monde à l'autre un peu trop brutal, il s'épousseta. Il fit ensuite quelques étirements et remit de l'ordre dans ses idées en plongeant son visage dans l'eau glaciale d'un ruisseau.

Les cheveux encore dégoulinant, il résuma la situation. Il ne pourrait plus utiliser sa magie, il ne porterait plus bonne fortune à ses protégés, mais il pouvait vivre avec eux. Il n'avait de toute façon jamais eut besoin de dons féeriques pour réparer, surveiller, raccommoder, puiser, nourrir et cuir.

La vue des feuilles mourantes entraînées par les rafales de vent lui fit comprendre que plusieurs semaines s'étaient écoulées depuis son départ. Il n'y avait plus une minute à perdre. Empoignant son bâton et réajustant la sangle de son sac, il reprit la route, n'ayant qu'une vague pensée pour son ancien monde, celle d'une promesse bafouée au peuple des Korrigan.

Quelques semaines plus tard, il retrouva Hélène, après avoir arpenté au hasard des nuits durant, les quartiers miteux réservés aux nouveaux arrivants sans le sous.

Comme la famille avait trouvé son équilibre avec les salaires d'employés d'imprimerie et de conserverie, Markus avait trouvé sa place dans les nouveaux d'élément de cuisine en bois gris, monté à la chaîne dans une usine du coin.

Et comme si de rien n'était, il recommençait son manège comme son père avant lui et comme son grand-père encore avant, il gardait le petit appartement perdu dans la ville.

***

Alors, ami lecteur, si parfois tu remarques que tes écouteurs sont étrangement démêlés le matin, l'ascenseur curieusement réparé dans la nuit, ou l'eau déjà chaude dans la bouilloire à ton réveil, pense à inspecter les fonds de tiroirs ou les recoins des placards.

Car si le monde des fées et des hommes sont aujourd'hui séparés, du moins le croit-on, il n'en fut pas toujours ainsi. Et on connaît l'obstination des Korrigans !