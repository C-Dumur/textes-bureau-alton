---
title: Plongée en mer de plastique
date: Décembre 2018
ordre: 4
summary: La petite embarcation rouillée s'arrêta, laissant son sillage tracé dans les cadavres de polymères se refermer derrière elle. Un océan de gris terne s'étendait à perte de vue autour de l'équipage ...
---

— Voilà, c'est là qu'ellle a été vu !

La petite embarcation rouillée s'arrêta, laissant son sillage tracé dans les cadavres de polymères se refermer derrière elle. Un océan de gris terne s'étendait à perte de vue autour de l'équipage.

— Et donc maintenant tu plonges, c'est ça ? T'es cinglée ! Tu peux encore changer d'avis. Commença Tom en s'approchant de sa coéquipière.

— Laisse tomber tu veux, je vais plonger que ça te plaise ou non !

— Mais tu vas crever, t'imagines même pas toutes les horreurs que tu vas trouver dans cette poubelle.

— Tom, je préfère mille fois mourir que de vivre dans un monde où elle n'existe plus !

Le jeune homme chercha du regard le capitaine de l'Aube, le bateau qui l'avait mené au milieu de cet enfer de déchets, cherchant un peu de soutien.

— Vous vous débrouillez entre vous, les jeunes ! Décréta Samuel.

Le jeune biologiste soupira profondément.

— Perso, je foutrais même un orteil avec des chaussures de sécurité là-dedans, mais je comprends Salomé. Si elle veut le faire, c'est pas moi qui l'en empêchera. D'autant que c'est grâce à votre fric que mon bateau n'est pas coulé au fond comme tous les autres. Je vous laisse gérer vos affaires.

— Parfait, conclu Salomé. C'est pour ça que votre rafiot s'appelle l'Aube et pas le Crépuscule, on va la trouver cette étincelle d'espoir ! À mes bouteilles !

Moins de dix minutes plus tard, elle était parée. Elle était engoncée dans une combinaison plus censé la protéger des résidus toxiques que de la morsure du froid dans cette mer réchauffée. Bouteille sur le dos, masque sur le visage et palmes aux pieds, elle lança un dernier regard à un Tom dépité.

Enfin elle traversa la pellicule de bouteilles jetées, d'assiettes jetables et d'accessoires superflus pour s'enfoncer dans une eau couleur cendre, tiédasse et visqueuse.

L'étendue des dégâts lui provoqua un haut le cœur.

Salomé, d'une nature silencieuse, avait toujours admiré le sens de commerce de son meilleur ami. À ce moment-là, elle maudit le sens des affaires de l'humanité.

L'humanité avait vendu le mouvement placide des méduses et les couleurs chatoyantes des coraux pour le mouvement placide des sacs plastiques et les couleurs chatoyantes d'un bouquet de pailles planté dans un gobelet de fast-food. Échangé la faune et la flore par le vide et la mort. Échangé la vie contre le plastique.

Pour ne pas sombrer dans le cynisme, elle décida de ne penser à dernière visite des fonds marins, à dernier passage dans son sanctuaire avant qu'il ne devienne un cimetière. Elle alluma sa lampe frontale et s'enfonça dans les profondeurs.

Arrivé au fond, un nouveau sable s'étendait devant elle. Un sable de synthèse aux couleurs délavées au contact de l'eau polluée. Le sol était devenu un amoncellement de paillettes de pétrole coloré auquel se mêlait le reflet rouillé des éclats de boites de conserves et de restes d'électroménager. Elle qui connaissait par cœur les ères géologiques et l'empilement de roche qui composait le sol de cette région, ne put s'empêcher de sourire tristement à la vue de la contribution humaine à cette ancestrale machinerie.

Elle refusait cependant de croire que celle-ci serait la dernière couche que l'Homme verrait.

Elle scruta les alentours, reconnaissant de-ci de-là, des fragments d'artefacts de la vie industrielle, bouteille d'eau gazeuse, flacon de gel douche, d'aérosol, carcasse de cafetière ...

Toboggan pour enfant ...

— Toi, t'es pas pensé pour être ici, pensa Salomé en décidant de s'approcher.

Devant la structure mourante de ce jeu, dressé comme la pierre tombale de son enfance, un frisson de nostalgie l'envahit. Quand soudain, un mouvement à sa base attira son regard. Par réflexe elle plongea sa main gantée dans la version 2.0 du sable marin pour en dégager deux capuchons de stylo bille tentant de fuir. Un nouveau frisson, d'effroi cette fois-ci, passa à la vue des pattes longiformes sortant de ce bout de plastique autrefois mâchouillé par un écolier.

Les quelques animaux survivants s'étaient adaptés, l'organique avait fusionné avec les déchets. Avec une grimace de dégoût, elle reposa sur le sol les deux nouveaux représentants des habitants de la mer. En s'enfonçant dans le sable pour fuir, un des crustacés perdit une patte et s'éteignit au sol en une fraction de seconde.

Même la faune était devenue jetable, l'adaptation était complète.

Oubliant le triste de sort du mutant, Salomé reprit sa recherche, elle tourna autour du jouet avec l'intime conviction d'être au bon endroit.

— Aller, où tu te caches ? Commença-t-elle à marmonner dans son masque en entreprenant de soulever l'abri présumé.

Retourner l'habitat des sujets à observer n'était pas dans les habitudes des biologistes en temps normal, mais pouvait-on encore parler de biologie ?

Alors que le pied en fer s'arrachait du sable, une bulle s'échappant du trou laissé l'inquiéta. Bientôt un liquide noir et épais commença à s'échapper. Salomé avait assez étudié les risques de sa petite balade aquatique pour savoir qu'elle ne devait pas rester ici. Elle avait peut-être découvert une réserve d'un quelconque polluant métallique. Elle ne pouvait s'en assurer et n'en avait de toute façon aucune envie.

Elle commença à nager vers la surface, non pas par peur de l'impact de la toxine sur sa santé, mais de ce que son odeur allait attirer. Elle avait assez observé de ces spécimens de nouveaux charognards, qui venaient se repaître des déchets industriels dans le fond de tous les ports d'Europe pour savoir qu'elle n'avait pas envie d'en rencontrer un.

Pressant la montée, sans pour autant oublier les notions les plus élémentaires de sécurités, elle scrutait autour d'elle à la recherche d'un éventuel dangers.

Elle se fit néanmoins surprendre.

Sur sa gauche, une horrible parodie de poisson chasseur était déjà sur elle. Malgré l'aspect industriel qu'avait le monstre, avec son œil gauche en capsule d'aluminium et les morceaux de PVC hérissant son dos, sa peau squameuse et suintante était répugnante. Comme si le peu de chose organique qui restait à l'ordre animal avait été corrompu par la pollution.

Elle évita juste à temps une morsure qu'elle imaginait terrible et contre-attaqua comme elle put.

Malgré la force que l'eau chargée exerçait sur ses mouvements, elle réussit à fendre le liquide dans un geste horizontal qui frappa à la mâchoire. Brisant les quelques couteaux en plastique et tordant la fourchette rouillée qui servait de dent au monstre. Il battit en retraite, laissant derrière lui un filet d'un sang vert qui s'échappait de sa blessure à la gueule.

Non, le monstre n'avait plus grand-chose d'animal.

Finalement, la disparition des proies vivantes des prédateurs avait du bon, elle n'osa pas imaginer comment elle s'en serait sorti si la gueule du poisson avait encore été adaptée pour déchiqueter de la viande fraîche.

Alors qu'elle se remettait de ses émotions, un filet de sang se diluait devant-elle. Par réflexe elle baissa les yeux sur ses bras cherchant une blessure.

Ses bras étaient indemnes. Ce n'était pas elle qui saignait.

Elle se retourna vivement et n'en crut pas ses yeux.

La Dernière, la dernière raie, le dernier animal marin n'ayant pas encore été pourris par l'Homme était devant elle. Le dernier animal ayant survécu à la grande corruption, ce que tout le monde croyait comme une légende passait devant Salomé ébahie.

Elle était blessée.

Elle était épuisée, affamée.

Elle était poursuivie par un abominable requin, assemblage foutraque de chaires pourrissantes et d'acier tordu, doté d'une gueule d'où s'échappait une salive de pétrole.

Mais elle était en vie.

Il restait un espoir.

Sans réfléchir Salomé activa sa balise de sécurité pour appeler son équipage, tira son poignard de plongé fixé à sa cheville et se jeta devant le prédateur.

Elle allait sauver le dernier espoir des fonds marins, ou mourir ...