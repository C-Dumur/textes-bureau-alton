---
title: Forces du Mal
date: Novembre 2015
ordre: 6
summary: L'odeur acre de la sueur, de la poussière, du mauvais tabac et de l'alcool frelaté empoisonnait l'air. Le parquet parsemé de tâche de graisse et de traces de brûlures témoignait du savoir vivre de clientèle de la taverne...
---
L'odeur acre de la sueur, de la poussière, du mauvais tabac et de l'alcool frelaté empoisonnait l'air. Le parquet parsemé de tâche de graisse et de traces de brûlures témoignait du savoir vivre de clientèle de la taverne. Le patron, un petit homme chauve au nez tordu et à la bouche édentée, riait à gorge déployée avec ses habitués tout en nourrissant un oiseau posé sur son épaule. Resk était la mascotte des lieux et pour rester raccord avec la première impression que pouvait avoir les visiteurs, la bestiole était antipathique au possible. Le vieux vautour obèse avait la même apparence que son propriétaire, même air crasseux et négligé, même regard torve et vicieux, on pouvait même compléter l'analogie en remarquant la tendance à perdre des cheveux pour l'un et des plumes pour l'autre. En bon charognard, il survolait souvent la pièce, dégradant l'hygiène, déjà contestable, de l'établissement pour fondre à l'occasion sur l'assiette d'un maraud. Une légende persistante dans une partie de la clientèle, la plus large, mais pas la plus fine, voulait que se faire piquer son morceau de viande par Resk était annonciateur de bonnes nouvelles.

C'est dans le fond de cette taverne pour le moins louche, qu'un groupe d'hommes tout aussi louches discutaient à voix basse en buvant dans des verres d'une propreté douteuse.

Cinq hommes assis sur des tabourets bancals arboraient toute une panoplie d'armes plus ou moins encombrantes, de tenues sombres et extravagantes et de bijoux représentant crâne, tête de mort, serpent ou dragon. Tous écoutaient parler un colosse à la barbe noire fournie, à la mâchoire carrée et au visage marqué par un réseau de cicatrices.

— Ça commençait bien, on a pris la capitale en moins de deux heures et on a passé la nuit à piller, massacrer la résistance et foutre le feu à tout ce qu'on pouvait.

— Bien fait pour leurs gueules ! Lança un homme au bout de la table.

— Vous avez trouvé beaucoup de richesses ? Demanda Teask qui posait toujours des questions essentielles.

— J'aurais donné n'importe quoi pour être à ta place. Continua Nerwin, plaintif, avant de remettre le nez dans sa choppe.

Guernold continua son récit.

— Et puis le lendemain on est tombé sur une armée de l'empire de l'est, on était deux fois plus, mais on s'est fait ... Il serra les dents, ce qui lui donna une allure encore plus sauvage.

— Battre ? Proposa Alastor en passant sa main dans sa barbe.

— Tailler en pièces ?

— Rouler dessus ?

— Capturer puis jeter en pâture à des sanglier de combat affamés ? Aida l'homme encapuchonné en bout de table, répondant au doux nom de Pirilius, dit « l'Ordure », très imaginatif à ce sujet.

Le guerrier fit non de la tête, prit une respiration et expliqua.

— On s'est fait poutrer la gueule !

Les mots étaient durs, même dans la bouche du brutal combattant. La phrase tomba comme un couperet, imposant un lourd silence à la tablée.

— Puuuuuuutainn ... Sifla simplement Teask entre ses dents.

— Dur... Conclut Nerwin de plus en plus désespéré.

Avec son visage d'ange, sa stature athlétique et ses habits de noble « à l'ancienne », Nerwin aurait pu avoir une vie heureuse de chevalier courtois chez les peuples libres. Mais il avait choisi les forces du mal, à l'époque prolifiques et avait échangé son bras, désormais maudit, contre la jeunesse éternelle à un démon. Il vivait très mal les échecs à répétition et tentait de noyer sa honte dans une infâme eau de vie vendue à prix d'or par le patron de la Molaire Cariée.

Alastor, nécromancien de son état, relança la conversation.

— Dis-moi, le héros qui t'a eu, il était de quel type ? Un jeune orphelin de quatorze ans qui galvanise les troupes avec son cœur pur et vertueux ? Il cracha à l'évocation de ces mots. Ou alors un vieux baroudeur bien coiffé qui s'est battu dans toutes les guerres depuis ses douze ans ?

— Un gamin, avant d'écraser mes troupes, il élevait des chèvres.

— Dur... Continua Nerwin dont les phrases se raccourcissaient à mesure que son verre se vidait.

Alastor tira de sa sacoche un horrible carnet dont la couverture de cuir était bouffée par les vers et prit ses notes. Depuis le début de sa carrière au service de Belbinor, démon de rang 4, il référençait scrupuleusement tous les héros qu'il avait affronté avec ses collègues. Il passait ensuite des heures à faire des fiches sur leurs points communs, sur les éléments qui poussent un pécore à sortir de la masse pour devenir un héros et à les classer par ordre de dangerosité. L'initiative ne servait à rien dans l'optique d'éviter la catastrophe à chaque bataille, mais elle donnait à Alastor la sensation de faire de vrais trucs de mage.

— Remarque, c'est pas les pires ... Repris Teask, il était le plus jeune de la fine équipe et avait encore foi en la puissance de l'obscure. Il était persuadé que les forces du mal écraseront un jour les peuples libres et alors, il sera le tyran despotique qu'il a toujours rêvé être.

Les raisons d'intégrer les rangs de l'obscur pouvaient être multiples. Soif de pouvoirs, vengeance, malédiction. Mais il fallait absolument un objectif à long terme pour supporter de travailler dans les terres maudites, avec des êtres vils et lâches sous la direction d'un démon sadique et lunatique. Teask n'avait pas besoin de ça, faire le mal et être une teigne avait toujours été pour lui une vocation.
Depuis tout petit, il frappait, volait et terrorisait son entourage. En bonne fouine qu'il faisait, il mettait, cela va de soi, un point d'honneur à frapper dans le dos, toujours les plus faibles et à aller se cacher derrière une brute pour éviter les représailles. Aujourd'hui il mettait à profits ses compétences innées pour mettre sur pied de redoutables pièges politiques ou des embuscades particulièrement retorses, sans plus de succès que ses collègues au style plus direct.


— Boarf... Fit Guernold, pas spécialement convaincu.

Lui ne s'amusait pas à classer les champions du clan ennemi par famille de gêneur. Il y avait les perdants d'un côté et les vainqueurs de l'autre et lui avait la sale tendance à se retrouver du mauvais côté. Mais tout cela n'était pas de sa faute, croyait-il, mais celle de ses bras cassés de collègues. Même si cette bande fillettes rachitiques le faisaient rire, il n'aimait pas leurs manières de faire la guerre, tout à base traîtrise et de coups vaches. Il n'y avait pourtant rien de plus simple que cette saine activité, on rassemble des hommes, on gueule avant de charger et on se sert dans les campagnes si on gagne ou en rentre chez soit lécher ses plaies si on perd. Merde, c'était très bien comme cela, c'est comme ça que l'on faisait à la bonne époque, mais non, il fallait toujours que ces foutus intellos compliquent tout avec leurs machinations foireuses et leurs stratégies à deux ronds.

Teask s'expliqua.

— L'avantage avec les « Loyal-bons », c'est qu'après leur heure de gloire, ils se retirent à la campagne pour vivre heureux et avoir beaucoup d'enfants. Le pire c'est les frimeurs aux grands cœurs — Son visage se tordit dans un rictus méprisant — En plus de se battre pour la justice et la liberté — Comme Alstor l'avait fait un peu plus tôt, il cracha par terre à l'évocation de ces mots qu'il détestait. — Ils reviennent à la charge à chaque fois pour déjouer nos plans. Un peu comme ce Chevalier de Lisawa.

La rage lui montait au nez, il serra son poing sur son verre qui craqua sous ses phalanges. Malgré une série de défaites toutes plus cuisantes les unes que les autres, il s'obstinait à croire qu'un beau jour il se débarrasserait du noble combattant.

— Tant que t'es dans ton foutu carnet, on en est à combien ? — Demanda Nerwin, qui, pour une raison qui lui échappait, tenait à s'enfoncer encore plus dans la morosité.

— Dix-huit échecs cette année — informa Alastor d'un ton étonnamment détaché, pareil à celui qui parle de météo.

En tant qu'érudit, il savait que la théorie des grands nombres jouait en leur faveur. Statistiquement, il arriverait forcément un moment où ils gagneraient. Même si l'explication, un peu fumeuse, charmait le groupe de choc, elle ne plaisait étonnamment pas à leur patron.

Pirilius, qui aimait remuer le couteau dans la plaie, revint à la charge.

— Vous avez des idées pour être moins ridicule la prochaine fois ? Dit-il en se rapprochant de ces interlocuteurs en baissant la voix, manœuvre objectivement inutile, personne n'écoutait ou ne s'intéressait à eux dans tout le brouhaha, mais bien pratique pour se donner des airs de conspirateur.

Il n'avait pas besoin de ça pour ne pas inspirer confiance à ses pairs. Même quand son visage n'était pas dissimulé par une large capuche ou par une étoffe rouge sang, il n'était pas le genre d'homme avec qui on avait envie de fraterniser. Avec son visage anguleux et maigre, son regard noir et menaçant, ses yeux de furet enfoncés dans leurs orbites et sa brûlure au coin de la lèvre qui lui déformait le visage et donnait l'impression qu'il était constamment en train de ricaner, il n'avait pas eu grand mal à se faire engager dans l'équipe de Belbinor. La simple énumération de ses goûts suffisait à s'assurer qu'il n'avait pas volé son titre, il aimait ses poisons, ses pièges, ses lames et ses outils de torture. Il faisait même parfois froid dans le dos à certains de ses collègues pourtant fort peu impressionnables. C'était à peu près tout ce que l'on savait sur «l'Ordure » qui avait méticuleusement effacé toute trace de son passé. Les plus mesquins disaient qu'il cachait un secret honteux, mais ceux-là n'étaient que très rarement cru, notamment, car médire de sur le compte de Pirilius entraînait souvent de fâcheuses complications telles que la baisse drastique de l'espérance de vie.

— Ouaip ... Grogna Guernold, Teask et Alastor hochèrent la tête sur le même air de confidence.

— Non, j'ai l'impression d'avoir tout essayé, gémit Nerwin et toi ?

Pirilius, trop heureux qu'on lui pose la question, eut un sourire carnassier. Il jubilait, une fois de plus, il allait briller par son intelligence maléfique.

— Oh que oui, fit-il en accentuant encore son sourire en coin. Il baissa sa capuche, fit mime d'hésiter pour faire languir son auditoire et savoura le regard fébrile de Teask et Alastor qui attendaient impatiemment son plan. Quand enfin il se décida à exposer son idée de génie, il fut coupé par la voix rauque de Guernold.

— Vos gueules, il arrive !

Le guerrier sans doute captivé par les hypothétiques révélations fracassantes de Pirilius, s'était mis à surveiller la salle et avait remarqué l'imposante silhouette qui avait fait irruption dans la taverne, avait commandé un verre avant de s'approcher d'un pas lent.

Tous se mirent à se sentir comme des renards pris dans un piège à loup sur le chemin d'un troupeau de bisons enragés : ils voyaient venir le danger de loin et ne pouvait rien faire pour y échapper à part fermer les yeux, serrer les dents et attendre que ça passe.

Enfin arrivé à leur niveau, Belbinor cracha pour toute salutations.

— Pitoyable, messiers !

La remarque eut l'effet d'un carreau d'arbalète, les cinq hommes enfoncèrent leurs têtes dans leurs épaules. Le démon s'assit sur le tabouret restant, mais le bois vermoulu émit un craquement sinistre sous son poids inhumain, aussi il préféra rester debout. Il toisa d'un regard mauvais la tablée, qui, pour se donner une contenance portait leurs verres à leurs lèvres. Belbinor les imita et grimaça. L'hydromel "du patron" était la spécialité de la Molaire Cariée et lui donnait son nom. L'élixir avait une allure pâteuse et était bien trop sucré, résultat de la trop grande quantité de miel bon marché servant à masquer d'autres goûts moins avouables.

— Infâme, fit il simplement.

Le démon profitait de son voyage annuel dans les plans supérieurs pour superviser ses inutiles, mais néanmoins seuls, généraux et surtout pour apprécier la saveur sucrée tout simplement inexistante en enfer. Il fut donc forcé de constater que la Molaire Cariée n'était pas le lieu le plus indiqué pour un voyage culinaire. Fâché de voir ses espérances sucrées gâchée par le mauvais choix de ses subalternes, il commença la revue de projet.

— Alors, ces peuples libres ?

— Ils le sont encore, grogna Guernold dans sa barbe.

Fidèle à sa réputation de brute épaisse, il enfonçait des portes ouvertes. La réponse fut moins cinglante qu'à l'accoutumée.

— Je sais bien bougre d'âne ! Pourquoi c'est encore le cas !

Il fut un temps ou Belbinor tenait sous sa coupe tous les royaumes de l'est, un temps où la populace devait le vénérer ou mourir et où les plus brillants esprits du mal se bousculaient pour intégrer son armée. Mais cela faisait des siècles que ce temps maudit était révolu, les seigneurs démoniaques avaient été repoussés dans les tréfonds de l'enfer, la moindre parcelle de terre avait été reprise par les peuples libres. Le mal ne faisait plus rêver depuis qu'une poignée de pécores avaient eu l'idée, plus que contestable, de sortir de leur situation de misère est de servitude et de prendre les armes contre l'oppresseur. Ceux qui auraient pût passer pour des fous idéalistes s'ils avaient été exécutés de façon salissante pour l'exemple, arrachèrent leurs premières victoires et posèrent les premières pierres d'un monde de justice et de liberté. Ces premiers héros, entraînèrent dans leur sillage tout un tas de nouveaux combattants avides de liberté, reléguant les derniers druides, les seuls à s'opposer aux ténèbres à l'époque en ce qu'ils auraient dû toujours être, une bande de tocards sénile, prétentieux et extrêmement rasoir et ce, malgré leur exubérante pilosité. Le mal était en crise, c'était indéniable et pendant que les plus fins bretteurs et puissants mages venaient grossir les rangs ennemis, Belbinor devait exploiter les rares ressources d'un guerrier aux méthodes passés de mode, d'un nécromancien limité, d'un prince dépressif, d'un pt'it gars inexpérimenté et d'un connard qui pouvait le trahir d'un moment à l'autre. Parfois, seul dans le noir des abysses, cette pensée lui faisait presque envie de prendre une retraite bien méritée dans un plan supérieur ensoleillé, avant que ses instincts teigneux et vindicatifs et son dégoûts pour toute forme de bonheur ne reprennent le dessus.

Diplomate, Alastor tenta de mettre les formes pour expliquer.

— Ben, c'est-à-dire que l'on a joué de malchance, on est passé à pas grand-chose à chaque fois et ce alors que nos ennemis deviennent de plus en plus puissants.

— Et vous de plus en plus mauvais ! Répliqua le démon d'un ton implacable. Il réajusta sa lourde cape d'une main crispée et tourna le dos à ses troupes.

D'une voix étrangement calme, il commença son énième discours chargé de menace.

— Vous n'êtes qu'une bande de larves inutiles et encombrantes, je mets à votre disposition des ressources en terre, en homme et en pouvoir et vous les dilapidez en manœuvres inutiles. Vous n'êtes même pas foutu de me prendre la moindre parcelle de champs à ces bouseux. Partout dans les Terres Libres vous êtes moqués. Les plus nobles princes comme les plus idiots des vilains attendent vos ridicules interventions un peu comme ils attendent le carnaval, comme une bonne occasion de rigoler et de faire un gueleton.

La voix du démon se posait et s'apaisait en même temps que sa colère montait, ce contraste glaçant contribuait à le rendre plus terrifiant encore.

— C'est-à-dire que ... Commença à grogner Guernold, avant de se taire devant les regards lourds de ces camarades. Le guerrier avait le sang chaud et contredire le démon pouvait avoir de désagréables conséquences.

— Vos conneries ont fait de mon nom, autrefois synonyme de terreur et de destruction, de mort et de flamme, une menace pour les enfants qui n'écoutent pas leurs parents. Je suis devenu un épouvantail, un vulgaire Croque-mitaine. Le voilà le résultat de vos travaux, du flan, du vide, zéro, sale, nul !

Il se retourna brusquement, ses yeux grenats irradiaient d'une lueur féroce. Pour finir sa tirade assassine avec panache, il fit claquer sa cape et descendit d'un trait son verre, le breuvage le fit tirer au cœur, pourtant mort depuis des lustres, avant de le jeter violemment contre le coin de la table. Le récipient éclata en morceaux grossiers qui manquèrent d'entailler le visage déjà peu engageant de Pirilius.
Le coup de sang du démon, n'attira qu'une réaction faiblarde du patron, passablement éméché.

— Hé ! Doucement avec le mobilier, les jeunes !

Nerwin, qui ne tenait pas à subir le courroux de Belbinor, usa du seul avantage qui lui restait pour satisfaire le patron.

— Vous en faite pas, il mettra sur mon ardoise, je n'suis plus à ça prêt.

La nature démoniaque de Belbinor le rendait peu enclin à la pitié. D'autant plus que la dépression de Nerwin avait commencé quand celui-ci avait fait tomber, après une manœuvre d'une intelligence toute relative, une partie des terres chaotiques sous l'emprise des peuples libres. Pourtant, la déchéance du prince corrompu ne l'amusait pas, il se reconnaissait à travers lui. Il était passé du statut de tyran éclatant et incontesté à celui de diablotin sans royaume en même temps qu'il avait vu « le prince démon » devenir une épave désespérée et alcoolique.

— En même temps, vu son état, je ne suis pas sûr qu'il se souviendra de quoi que ce soit.
Espéra Teask pour qui il n'y avait pas de petite économie. — On aura qu'à cacher les éclats et ni vu ni connu.

Belbinor rebondit sur cette proposition désintéressée pour revenir à la charge.

— Et à part jouer à la bonne pour cacher vos ô combien terrifiants crimes, vous comptez faire quelque-chose d'un tantinet utile ou je vous cherche un boulot d'éleveur de poules ?

Alastor n'eut pas le temps de répondre par ses sempiternelles formules mielleuses et administratives, déjà Guernold grogna d'un ton chargé de fiel.

— Évidemment !

Toute la tablée continua de se décomposer, en revue de projet, il fallait à tout prix empêcher le guerrier de s'exprimer, en digne représentant de son peuple, celui-ci avait une approche de la diplomatie pour le moins abstraite.

Fâché par l'irrespect de son équipe, le démon fut prompt à rétablir son autorité d'une remarque acide.

— Évidemment ? Votre motivation et vos compétences me semblent tout sauf évidentes ! Mais soit, je suis tout ouït.

Un silence de plomb tomba, le même silence qui prend aux tripes les soldats avant une bataille perdue d'avance.

Teask, comme à l'accoutumée, persuadé du génie de son plan, l'exposa avec une conviction presque naïve.

— Le problème ce n'est pas nous ...

L'orateur fit semblant de ne pas avoir entendu le « Si » lâché à la volée par le démon déjà peu convaincu par la suite du discours.

— Mais bien la multitude de nouveaux héros qui viennent nous mettre des bâtons dans les roues.

— Déjà bien embourbées, continua Belbinor d'un ton absent, comme s'il avait parlé sans même y penser.

Malgré tout, Teask continua.

— Ils ont poussé comme des champignons vénéneux ces dernières années. Et bien c'est simple, on lance l'opération « Épuration », une grande vague d'assassinats contre tous ces loyal-con. Une fois tranquille on lâche les chiens et on reprend le contrôle.

Une vague de perplexité envahit l'assemblée. Plus le silence durait, plus le sourire un peu niais de Teask se déformait. Guernold mit fin au supplice grâce à son légendaire esprit de synthèse.

— On va dans le mur, mon petit loup !

Chose extraordinaire, le démon appuya le propos d'un de ses généraux.

— C'est clair ! Il y a combien de cibles potentielles ?

Tous les regards ce tournèrent vers Alastor, qui, d'un geste nerveux enfonça son carnet dans son sac avant de feindre l'incompréhension. Il fallait éviter d'énoncer de mauvaises nouvelles à ce moment précis. Et puis Teask lui était sympathique, il ne tenait donc pas à l'enfoncer d'avantage.

L'Ordure, étonnamment, n'eut pas cette sollicitude. La présentation maladroite de Teask était du pain bénit pour lui, plus il discréditerait les plans de ses petits camarades, plus, par contraste, le sien semblera brillant.

— Et qui tuera cette bande de cons, hein ? Commença-t-il avec son sourire cruel, ta bande de pirate d'eau douce ? Fait-moi pas rire, j'ai horreur de ça.

— Sans compter que ...

La surprise fit grossir les yeux écarlates de Belbinor, quand, pour la première fois, Alastor, le plus obséquieux de la troupe, le coupa.

— Je sais pas, tu n'arrêtes pas de te vanter de ta cruauté, prouve nous que ce n'est pas que du vent !

Le mage lança un regard noir à Pirilius. Il voyait clair dans son jeu et tenait à le lui faire comprendre.

— On sait que t'es un connard, mais ça ne t'empêche pas de participer à nos efforts de guerre !

L'Ordure était le moins apprécié de ses collègues. Teask et Guernold le faisaient régulièrement savoir de façon plus ou moins subtile, mais c'était la première fois qu'Alastor lançait une charge directe.

Pour toute réponse, Pirilius passa son pouce devant sa gorge, le mage, peu impressionné, le défia du regard. Le démon s'inquiéta de cette réaction vindicative du mage. Il se devait de lui rappeler son rang.

— Et toi, Alastor, une idée géniale à nous présenter ?

Il hocha la tête et tira un rouleau de parchemin graisseux de sa sacoche et le déroula sur la table tout aussi graisseuse. Le document était recouvert d'horribles symboles, de dessins terrifiants et de traces de bave laissées par l'auteur à moitié fou. On était dans le domaine de la magie noire foncée, ça ne faisait aucun doute, tout cela s'annonçait donc fort prometteur.

En réalité, même Alastor n'avait pas une traître idée de ce que racontait ce parchemin, mais présenter un document, même factice, avait le bon goût d'aider à se faire passer pour un type sérieux et compétent.

— Je vous présente la clef de notre succès, commença-t-il en pointant la représentation d'un pentacle malsain et tordu.

Il eut pour toute réponse une volée de regards perplexe. « Heureusement que Belbinor est un démon de bas rang qui ne sait pas lire la langue des anciens. » pensa le mage en guettant l'étincelle d'incompréhension dans le regard de son patron, si sa supercherie venait à tomber à l'eau, il gagnerait sans doute un voyage tout frais compris en enfer.

— Ce parchemin décrit un vieux rituel permettant d'invoquer le terrible Azrctor, une créature extra-planaire.

Voyant que la perplexité n'avait pas quitté l'œil vif de son auditoire, il explicita.

— Et bien, il ne nous restera plus que lâcher la bête sur nos ennemis et les regarder se faire bouffer en mangeant des biscuits.
C'était tout simplement diabolique, Alastor se gonfla de fierté en voyant un sourire entendu apparaître sur le visage de Guernold, Teask commença même à entonner un rire machiavélique.

La félicité ne fut pas de longue durée.

— C'est non ! Coupa Belbinor sans quitter du regard le parchemin. Teask étouffa son rire dans un couinement désagréable, semblable à celui d'un furet dont la queue se serait coincé dans la porte d'un clapier.

Une nouvelle fois, l'ambiance était à couper au couteau.

Alastor, décidément d'une humeur moins servile que d'habitude argumenta.

— Mais attendez ! C'est pas n'importe qui, Azrctor ! C'est « Celui qui souffle la mort », « La Chose par delà les enfers » « Le Fléau des Dieux ». Un monstre colossal, deux fois plus grand que les dragons millénaires, des écailles aussi dures que la pierre, un souffle acide capable de faire fondre le granit et une rangée de dents longues comme des claymores !

Guernold compléta l'analyse de son proverbial sens de la formule.

— Bref, un truc qui envoie du pâté !

— C'est non ! Repris le démon sur le même ton monotone.

— Mais pourquoi ?

— Parce que !

Ses yeux s'illuminèrent d'un violet malsain, une légère nappe de fumée commença à virevolter autour de son visage et sa cape claqua comme par magie.
Les quelques artifices pompeux de Belbinor, à l'image de la hauteur de vol des hirondelles, étaient un indicateur relativement fiable de l'arrivée d'une pluie de tracas divers et variés. Aussi, le mage décida d'abdiquer.

— Très bien.

— Parfait !

Par expérience, Belbinor savait qu'il était risqué de charger Alastor de la réalisation de complexes rituels magiques, tant sa philosophe de « l'à peu près » et du « C'est presque pareil » pouvait se relever légèrement dangereuse pour tous dans un rayon de cinquante kilomètres, un peu plus les jours de grands vents. De plus, il savait de source sûre, que les monstres importés directement depuis les plans chaotiques avaient la fâcheuse tendance à être belliqueux et avides de pouvoir et ne tenait pas particulièrement à avoir un nouveau concurrent. Enfin la raison la moins avouable était celle ayant le plus de poids était la peur du démon. Si cette bande de pignoufs, aussi incompétents soient ils commençaient à s'amuser à invoquer des esprits maudits, il y aurait forcément un moment où l'idée de les révoquer leurs viendrait et avec celle-ci, celle de se débarrasser du patron, il serait alors condamné à pourrir en enfer à tout jamais.

Même s'il avait des raisons louables, il estimait ne pas avoir à expliquer son refus à son petit personnel.

— C'est dommage, quand même, continua Guernold.
Une grande lassitude envahit le regard du démon qui pensait le débat clos.

— Laisse, c'est pas grave lâcha Alastor qui voyait d'un mauvais œil la prolongation des parlementions, rien ne pouvait faire changer d'avis l'implacable Belbinor.

— Guernold, je suppose que toi aussi tu as une idée foireuse à nous exposer ?

Un frisson parcourut l'assemblée, le moment était critique. Si l'on évitait l'accident diplomatique entre le brutal guerrier et le démon, alors la soirée serait sauvée, sinon, et bien advienne que pourra...

— J'ai bien réfléchi.

D'humeur taquine, Pirilius anticipa la réaction du patron.

— Noooooooon, soufla-t-il avec un sourire en coin.

Le trait d'esprit, en tout point drolatique, n'eut pour seule réponse un geste dont l'obscénité pousse la bienséance à ne pas le décrire ici.

— Si cette bande de boufons nous rétame à chaque fois ....

— C'est parce que vous êtes des ...

Cette fois-ci le guerrier reprit la parole en le recoupant immédiatement.

— Des chevaux dirigés par un âne !

À ce moment, toute la tablée eut la soudaine envie de se liquéfier sur place pour éviter d'être témoin de ce qui allait se passer.

Le regard de Belbinor gagna encore en luminosité, le faisant de plus en plus ressembler à un lampion citrouille. Si la remarque avait été lancée par un autre, il aurait dominé de toute sa hauteur l'impertinent et lui aurait ordonné de répéter en le regardant dans les yeux. Hélas, Guernold était un colosse aussi grand que lui et avait la subtilité d'un coup de genoux dans la mâchoire, il aurait donc répété sans se dégonfler. Aussi, le démon préféra se taire et fulmina une vengeance atrocement fourbe et douloureuse.
Pas plus impressionné, le guerrier continua.

— Je disais donc, si on se fait poutrer à chaque fois, c'est parce que nos ennemis travaillent ensemble, alors que nous, nous nous contentons d'attaquer chacun de notre côté. Si on rassemble toutes nos troupes, mes orcs, les morts-vivant d'Alastor, les guerriers de Nerwin, les malandrins de Teask et les péquenauds de l'autre connard, ça nous fait une armée de ...

— Presque quinze mille combattants, fit Alastor qui était le meilleur en calcul mental.

Toutefois, un murmure de réprobation envahit le petit comité. Tous venaient de se voir embarquer, eux et leurs troupes, sans leur consentement, dans une campagne qui fleurait bon l'échec programmé.

— Vous imaginez, continua Guenold, emporté par des élans épiques, quinze mille créatures vomies des terres maudites déferlant sur les rivages du sud, guidés par les cinq plus grands généraux chaotiques. Ils vont chier dans leurs froques.

— Terrible... continua Belbinor en haussant les sourcils avec mépris.

Le visage du combattant rougit, il était déjà à la recherche d'une nouvelle pique à lancer la hiérarchie. Il n'eut cependant pas le temps.

— Et c'est tout ? Tu plaisantes j'espère ? Ton alliance de blaireaux ne serait pas capable d'arracher aux lutins leurs foutues collines. Tu n'as pas été foutu de prendre les collines aux korrigans Guernold ! Tu n'as pas pris la moindre parcelle de terre à une bande de demi-portions qui vont pied nus armés de frondes et de lance-pierres, tu es un incapable !

C'était fini de la quasi-bonne ambiance de la soirée. Le guerrier se leva d'un bond, tous les verres présents sur la table se renversèrent. Il s'égosilla.

— Tu sais pourquoi on n'a pas pris les collines ! Car tout le monde aime les lutins ! Tous ! Même l'Ordure aime les lutins !

Le principal intéressé fit non du doigt, Guernold lui lança un regard signifiant à peu de choses près « On en rediscutera plus tard mon petit loup, commence à numéroter tes dents ».

— Si on marche vers eux, tu peux être sûr que toutes les armées des Terres Libres vont se lancer à leur secours, on ne peut rien faire à un contre cent-cinquante. Tu comprends où il faut faire un schéma ?

— Tu viens de m'avouer que vous ne pouvez rien faire contre une bande d'êtres minuscules, vous êtes pitoyables.

Ce n'était pas vraiment les termes de Guernold, mais la mauvaise foi était un ingrédient indispensable pour rendre une soirée plus festive.

C'en était trop pour le combattant qui balaya définitivement l'espoir de terminer cette discussion dans le calme et la sérénité.

— Je ne vois même pas pourquoi je parle stratégie avec un diablotin qui passe ses journées le cul au chaud dans ses souterrains et qui se réveille tous les six mois pour venir nous les briser ! Tonna le guerrier hors de lui. Un démon qu'on ne voit jamais sur le champ de bataille et qui boit de l'hydromel, étonnant qu'il soit si peu respecté !

Belbinor vida tout l'air de ses poumons dans un sifflement rauque. Lui aussi se leva d'un coup et les deux opposants se fixèrent d'un regard chargé de haine et de mépris.

L'ambiance devient électrique, le temps semblait s'écouler trois fois plus lentement qu'à l'accoutumé. Belbinor et Guernold étaient si proches que le souffle alcoolisé du guerrier se mêlait à celui putride du démon. Leurs visages étaient crispés, tout leurs muscles tendus, leurs respirations saccadées. La scène rappelait le duel entre deux taureaux. Le premier qui baisserait les yeux serait condamné, la moindre parole, le moindre geste était susceptible de provoquer un bain de sang.

Les autres attendaient fébriles la fin de cette silencieuse, mais terriblement violente confrontation. Elle ne pouvait pas se finir autrement que dans le sang. Ils devaient choisir rapidement à qui il devait se rallier. Affronter le courroux du démon en s'opposant à sa tyrannie où penser à sa carrière en éliminant Guernold et passer le reste de sa vie à la botte de Belbinor. Tous savaient que leur destin allait se jouer dans cette taverne sentant le graillon.

Et la confrontation dura...

Dura...

Jusqu'à l'attaque de Guernold.

— Enfoiré ! rugit-il en renversant la table sur son adversaire.

Il ne comptait pas blesser l'être démoniaque comme cela, c'était, il l'espérait, la dernière provocation, celle qui mettrait feu aux poudres. Ce ne fut cependant pas le démon qui explosa.

— On a dit doucement les jeunes ! Gueula à plein poumon le patron de l'auberge. Vous êtes con où vous le faites exprès ?

C'était sans doute les litres de bière qu'il avait derrière le tablier tâché qui parlaient à ce moment. Le tavernier était habituellement plus poli avec les clients armés jusqu'aux dents, ce qui n'était pas totalement idiot quand on tenait à la vie.

— La ferme ! — Rugit Belbinor à l'attention du poivrot — Reste en dehors de ça !

Puis il se tourna vers Guernold.

— Tu vas payer, misérable mortel !

Un flou artistique tomba alors sur la taverne, tout alla vite, très vite.

Guernold plongea sur son épée, Belbinor sur Guernold. Teask tira sa lame. Alastor saisit son bâton. Un cri retentit.

Le poing du démon s'écrasa sur le visage du guerrier. Belbinor sentit une douleur cuisante dans son dos. Reask, le vautour du patron, s'était lancé vers les perturbateurs pour calmer le jeu. Il planta ses serres dans la chaire morte de Belbinor. Guernold en profita pour s'échapper de son emprise. Teask, carriériste, se lança sur le guerrier pour lui faire payer. Il fut interrompu par une douleur à la mâchoire. Alastor venait de s'interposer entre Teask et Guernold et protégea son brutal ami à grand renfort de coups de bâton. Nerwin pleurait dans son coin. Rendu encore plus hargneux par la douleur, Belbinor attrapa l'oiseau et le frappa sur le sol.

Reask émit un couinement pathétique en disparaissant dans un nuage de plumes ternes. La foule émit un grondement. Leur mascotte venait d'être agressée. Nerwin continuait de pleurer. Tous les badauds s'élancèrent dans la mêlée, prêt à venger le saint oiseau. « Traître » cracha Alastor en abattant une nouvelle fois son bâton sur Teask.

La dague de Pirilius se planta dans la gorge de Nerwin, la luxueuse épée ainsi que la bourse du prince rejoignirent le sac de l'Ordure. Belbinor tenta d'affronter la horde d'ivrognes vengeurs. L'Ordure disparue dans l'ombre. Belbinor tomba sous les coups de bâtons.

La dernière menace qui planait sur les peuples libre disparue cette nuit. Éliminé par une troupe de paysans dopés au picrate.

Teask finit ses jours dans un cachot. Alastor et Guernold utilisèrent la prime de sa capture pour se payer des vacances bien méritées par delà les mers. L'Ordure s'était tiré avec la caisse de la taverne. Jamais on ne sut en quoi consistait son plan. L'histoire ne saura jamais que les Terres Libres étaient passées à très peu de choses de leur destruction pure et simple. Elle retint cependant que c'est un vautour répondant au doux nom de Reask qui, par son sacrifice, mit à bas le démon. 