---
title: Menu de Fête
date: Juillet 2021
ordre: 2
summary: Tante Mixture, comme souvent, courait dans tous les sens. Elle lança une brassée de bois dans la cuisinière, inspecta sa vaisselle des grands jours ...
---

Tante Mixture, comme souvent, courait dans tous les sens. Elle lança une brassée de bois dans la cuisinière, inspecta sa vaisselle des grands jours, passa un coup de pierre sur son couteau et débarrassa son plan de travail d'un coup de bras rageur. Zelif L'Oisif, le chat de la maison, lâcha un miaulement boudeur, il savait déjà qu'il allait être emporté contre son gré dans cette agitation.

Il était évident que l'argenterie avait besoin d'un sérieux coup de vaisselle, Mixture se chargea donc d'un grand bac qu'elle partait remplir au ruisseau du coin. Elle profita de son passage par le salon pour déplier une grande nappe noire sur la table et ensorceler son balai pour qu'il fasse un brin de ménage.

— Et va me chercher mon courrier, ordonna-t-elle. Et tu pourras me faire les carreaux aussi ?

Elle voulait que tout soit parfait pour le soir, cela faisait si longtemps qu'elle n'avait pas réuni sa petite famille pour la fête de Samain. Elle avait hâte de voir ses enfants et petits-enfants. Toutefois, un problème se posait encore. Que leur faire à manger ? 

Occupée par son travail quotidien de guérisseuse du comté, ses obligations auprès du “Comité des Enchanteurs Fantasques Mais Compétents” et ses compétitions de tir à l’arbalète, elle n'avait pas eu le temps de composer un véritable menu de fête. Elle comptait désormais sur une inspiration soudaine. Hélas, ni l'air automnal ni la lumière dorée se reflétant sur le ruisseau ne lui provoqua de révélation. Elle revient donc en cuisine avec de l'eau, mais toujours à sec d'idée.
Retirant son vieux chapeau pour s'éventer le visage et l'esprit, Mixtures déclara :

— Tant pis pour l'originalité, plat traditionnel de Samain, ça fera plaisir aux petits !

Elle tira son couteau et regarda par la fenêtre son potager. Ses courges, citrouilles et potirons, aidés par les engrais plus ou moins magiques qu'elle leur prodiguait, étaient de tailles “respectables”. Au moins suffisamment pour dissuader les éventuels voleurs de s'en approcher, à moins d’être trois pour les soulever. Les légumes feraient une garniture de tarte parfaite.

La sorcière ouvrit son placard pour en sortir son plus grand chaudron, mais une odeur désagréable s'en dégagea. Elle réfléchit quelques instants pour se souvenir de ce qu'elle y avait fait bouillir la dernière fois. Elle grimaça, la marmite avait accueilli la préparation de la fameuse potion "Repousse Enquiquineurs et Inspecteurs des Impôts" composée d'ingrédients forts odorants et peu ragoûtants qui souillaient encore l’étain. Elle ne pouvait pas cuisiner dans cette marmite.

—Zelif, s'il te plaît, tu veux bien nettoyer ça pendant que je vais chercher les légumes ? Merci bien ! Lança à la cantonade Tante Mixture.

Le pauvre chat eut à peine le temps de couiner que la sorcière était déjà sortie. Il se mit donc au travail avec un acharnement tout relatif.


En poussant la porte de sa remise pour y prendre une hache de bûcheron, outil plus dimensionné à la taille de sa récolte, Mixture fut envahie d'un doute. Sûrement que le menu plairait à sa fille aînée, Châtaigne, devenue sorcière officielle de l'automne, mais l'autre branche de la famille risquait de moins apprécier.

La fille cadette de Mixture, Harpie, avait hérité du sale caractère et de la figure repoussante des sorcières d'autrefois. Elle s'était mariée avec Mwelor Chair-de-cendre, un nécromancien désagréable, arriviste et dénué de toute forme d'humour. Ils formaient depuis une famille pas si heureuse vivant au fond des marais. Élever des enfants dans une terre aussi hostile était contraire à tout bon sens et Mixture craignait que ses deux petits fils, Viscère et Entraille, maigres et maladifs, ne se transforment en vampire... ou en grenouille...

Elle poussa le portillon de son potager et en ressortit aussi vite quand la plus grosse citrouille se mit à feuler d'un ton menaçant à son approche. Cette fois-ci, c'était sûr, Mixture n'avait plus à se soucier des chapardeurs dans son jardin. Peut-être avait-elle trop insisté sur les engrais de son invention ? Elle se nota d’organiser, à l'occasion, une contre-révolte légumière avec son arbalète et retourna à ses problèmes de fête. Elle était de nouveau au point mort et son menu n'avançait pas.

Elle se réconforta en pensant que la tarte à la citrouille n'était finalement pas une si bonne idée et envisagea de s’inspirer de la gastronomie des marais. Après tout, salade de mandragores et d'algues, brochettes d'ailes de chauve-souris aux champignons vénéneux et sorbets de limaces n'étaient pas si mauvais.
Mais, une fois de plus, elle abandonna l'idée. Sa dernière tentative avait plutôt mal tourné. Pendants les dernières vacances d'été, Mixture avait essayé de faire plaisir à Viscère et Entraille dont elle avait la garde pendant une semaine en leur préparant un joli crapaud confit. Hélas, le temps de préparer le caramel, elle s’était liée d’amitié avec le batracien bavard et ne put se résoudre à le jeter dans la marmite. Depuis, Grande-Gueule Numéro Deux, comme elle l'avait baptisé, habitait avec sa petite famille dans un bassin de l'arrière cour. La sorcière estima donc qu'elle n'avait plus assez de place dans ses placards pour en faire une collocation de chauve-souris et recommença à faire vagabonder son esprit.

Elle pensa à la jeune Pomme, la fille de Châtaigne, partie en septembre faire de prestigieuses études de magie blanche dans la capitale.
Voilà ce qu'allait faire Mixture, des plats déroutants et luxueux comme le font les chefs étoilés citadins et leurs grandes toques ridicules. Pour ça, elle pouvait compter sur un détournement de son armoire d’apothicaire, elle n'avait qu'à choisir ses composés les plus goûtus et les assembler pour en faire des plats aux saveurs exotiques.
Après tout, si elle les utilisait dans ses remèdes, cela ne pouvait pas faire de mal en cuisine. Elle soupçonnait déjà les sales mioches du village de forcer leurs toux auprès de leurs parents pour profiter des sirops de miel et de violettes de la sorcière, tout cela était donc fort logique.

Elle allait parsemer ses sauces et crèmes de plantes médicinales qui allaient donner des vitamines aux pauvres enfants palots qu'étaient Viscère et Entraille et décupler la concentration de Pomme pour ses longues soirées d'étude.
Elle chassa également l'idée de saisir l'occasion pour se débarrasser de son infâme gendre d'un coup de mélange de plantes "hasardeux".

Elle qui avait inventé plus de trente filtres et potions, publiait régulièrement ses recherches dans de grands journaux à comité de lecture et avait écrit deux grimoires qu'on s'arrachait en librairie, venait d’avoir la meilleure idée de sa vie.

Elle entra triomphante dans la cuisine, l’esprit emplit de fièvre créatrice. Elle hurla au chat :

— Zelif, j'espère que tu es en forme, car tous les deux on va révolutionner la cuisine ! Puis on ira donner des conférences dans le monde entier !

La réponse du chat fut plutôt faiblarde et prit la forme d'un léger ronflement. L'Osif s'était lové dans le chaudron puant sans même y donner un coup de chiffon.

Mixture appela son balai enchanté et lui fit signe de se mettre à l'ouvrage. Celui-ci, un chiffon noué comme un tablier autour de son manche, déposa le courrier de la sorcière sur la table et commença. Il frappa impitoyablement l'étain du chaudron, provoquant une onde sonore qui réveilla brutalement le chat qui en sortit d'un bond, le poil hérissé.

Mixture, dont l'excitation descendait aussi vite qu'elle était montée, parcourut du regard son courrier. En femme très occupée, elle avait reçu ce jour-là deux invitations à des congrès, la lettre d'une consœur lui faisant part de ses dernières découvertes, une demande d'interview d'un journaliste du coin, une facture, une carte postale d'un camarade rencontré lors d'un stage de druidisme, une contravention pour stationnement de balai gênant et un étrange bout de parchemin sur lequel son regard se perdit de longues secondes.

— Finalement, déclara-t-elle, l'important c'est qu'on soit ensemble, non ? Grande Gueule Numéro Un ?

Aussitôt un battement d’aile se fit entendre entendre et un corbeau se posa sur son épaule. Elle lui arracha une plume sans qu'il en s'en offusque, la trempa dans un pot de jus de myrtille et griffonna quelques mots sur l'étrange parchemin. Avec un morceau de ficelle à rôti, elle l'attacha à la patte de l'oiseau et celui-ci parti sans demander son reste.

Il vola un long moment vers la ville et se posa sur le comptoir d'une échoppe. Un apprenti décrocha le message, le parcourut du regard et jeta une noix à Grande Gueule Numéro Un.

— C'est noté, mon grand !

L'oiseau attrapa au vol son pourboire et quitta le magasin, sur la devanture duquel on pouvait lire :

***
La Baguette et la Spatule,
Traiteur magique depuis 1494,
Mariage, Baptême, Anniversaire, Victoire de l'équipe nationale,
Livraison à dos de dragons toute l’année et sur tout le comté.
***