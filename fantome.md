---
title: Fantôme
date: Juin 2022
ordre: 1
summary: Cédric avait trente-deux ans, avait fait des études d’histoire qui l’avaient profondément ennuyé, pesait soixante-treize kilos, avait dix sur dix à chaque œil, mais allait perdre l’audition d’une oreille...
---
Cédric avait trente-deux ans, avait fait des études d’histoire qui l’avaient profondément ennuyé, pesait soixante-treize kilos, avait dix sur dix à chaque œil, mais allait perdre l’audition d’une oreille. 
Il avait peu d’amis, était éloigné de sa famille et la situation lui pesait. Depuis quelques semaines, il se sentait froid, morne et vide. Il envisageait devenir végétarien et de quitter son boulot. Tout cela n’était qu’une microscopique parcelle de ce qu’on savait sur lui.  Comme pour tous ses concitoyens, on connaissait ses habitudes, ses croyances et ses convictions. Le Système Automatique de Surveillance lui avait attribué le label « A surveiller, Niveau trois sur six »

Comme on l’avait déjà prédis, il allait faire ses courses hebdomadaires. Il n’avait pas fait de liste, mais avec une analyse de ses derniers achats, un ciblage sur sa classe professionnelle ainsi que la lecture des dernières publicités qu’on lui avait administré, le grand distributeur savait déjà de quoi il remplirait son panier.

Il parcourait les froides allées de béton sans chercher à éviter les caméras à reconnaissance faciale. Il y avait un moment déjà qu’il s’était résigné. La ville était de toute façon quadrillée de mailles trop serrées pour espérer en réchapper sans attirer l’attention. Couvrir son visage dans l’espace public était passible d’amende et les caméras reconnaissaient de toute façon les corpulences et les démarches. Il fallait accepter d’être suivis ou rester cloîtré chez soi.  Alors, toutes les nuits, dans ses rêves, le dernier bastion où on ne pouvait pas encore le surveiller, il enviait les mystiques et les marginaux, qui vivaient seuls et protégés de ce monde qui devenait fou. 

Le pire dans tout cela, c’est que la large majorité des citoyens applaudissaient des deux mains. Il est vrai que depuis qu’on avait « ré-habité la ville » et « repensé l’habitat » chacun voyait ses besoins satisfais avec une rapidité et une simplicité spectaculaire. Attendre, hésiter, s’interroger ou douter était au mieux désuet, au pire une perte de temps dans une vie bien remplie. 
La criminalité avait également baissé en flèches, l’anonymat étant bien peu compatible avec la connexion et la surveillance permanente instaurée. Dès que l’Algorithme détectait le moindre premier cahot dans le parcours de vie d’un honnête citoyen, le Système de Surveillance en était notifié. Alors, à coup de pédagogie, aidé par les « recommandation pour vous » et le « contenus personnalisés » on remettait la pauvre brebis dans le droit chemin. Ce système évitait souvent d’en arriver aux pistolets tranquillisant et aux matraques.

Bien sûr, il y avait parfois des trous dans le filet, mais les cavales des derniers grands bandits permettaient de raconter des histoires à faire frémir les honnêtes gens. Hélas, il y avait aussi ceux qu’on qualifiait de faux-positif, des innocents catalogués à jamais comme dangereux, les coupant à vie d’une partie de leurs droits. « On ne fait pas d’omelette sans casser les œufs » avait dit un jour à ce propos le ministre de la Coopération et de la Convivialité. Se poser des questions était de toute façon une perte de temps et plus personne n’avait de temps à perdre. Il y avait toujours plus de travail à abattre, plus de programmes vidéo à dévorer, de jeux à consommer, de conseils développement personnel ou bien être à appliquer et de points de réputation à gagner sur les réseaux sociaux. La vie était bien trop courte.

Il regarda le bracelet électronique de sa montre connectée, lui non plus n’avait plus beaucoup de temps, comme les autres, il pressa le pas.

Il passa devant l’écran publicitaire géant au bout du boulevard. 

Ce panneau était le symbole de tout ce qu’il abhorrait. Bardé de caméras, il représentait l’intrusion même de la publicité ciblée au cœur des villes. À chaque citoyens qui passait, une des tuiles luminescentes s’animait pour afficher un message publicitaire automatiquement sélectionné. Malgré tous ses efforts pour ne pas prêter attention à l’écran, son cerveau de primate, attiré par l’irrésistible stimulus, attarda son regard. La bande annonce d’un documentaire vantant l’apport des multinationales au monde actuel, défila devant ses yeux. Au même moment, il reçut sur sa montre une notification lui annonçant qu’une réduction était en cours sur l’abonnement au service vidéo proposant le film. Dans ce monde actuel, avec l’apport des multinationales, on devait payer pour recevoir son propre bourrage de crâne.

Il se souvenait, presque avec nostalgie, des premiers jours de ce panneau, quelques années auparavant. Encore mal calibré, celui-ci affichait au grand jour par ses annonces bien trop ciblés, les désirs les plus profonds et les plus sombres de la population. Un scandale qui avait fait se masser autour du panneau une foule armée de battes, venus exiger son démantèlement dans les plus brefs délais. 
Bien sûr, on avait gagné. On avait gagné quelques semaines de répit et une centaine de mètres de terrain.  Au début de l’année suivante, le même panneau en version 2.0 avait été inauguré une rue plus loin. On avait fait ça en grande pompe, avec discours des cravatés, intervention d’expert venu vanter les progrès technologiques, champagnes et petits fours. 
La même nuit, on avait accueilli les derniers réfractaires venus en découdre avec gaz au poivre et procès pour « Tentative de dégradation de biens communs »

Arrivé à proximité  du méga-marché, une voix bourru le héla.

— Contrôle d’identité !

Un colosse à la mine patibulaire, logo du leader sur le marché de la sécurité sur blouson de cuir, s’approcha. Il y avait des années que la sécurité aussi, avait été ouverte à la concurrence. 

Cédric obtempéra sans faire de vague, il connaissait bien les loustics employés par cette firme et avait assez d’ennuis pour le moment. Il tendit son bracelet et le garde le scanna. Sur les lunettes fumées de l’homme, défila alors la vie de Cédric. Il se rassura en pensant que le colosse était encore plus fiché que lui par son employeur boulimique de données. Il était de notoriété quasi-publique que les employés des multinationales, sous prétextes « d’innovation dans les méthodes de management », utilisaient leurs collaborateurs comme premiers cobayes.

Soudain, un reflet rouge apparu à la vue du garde. Il venait d’être informé du niveau de surveillance qu’on avait affublé à son interlocuteur. Il était alors de son devoir de pousser le contrôle, la prise d’un individu dangereux lui apporterait peut-être des points à échanger sur la boutique virtuelle de son entreprise.

— Vous permettez ? Fit le colosse en pointant l’objectif de son téléphone sur le possible contrevenant.

Avec un humour cynique, Cédric esquissa un salut de la main à la caméra, depuis le temps, il connaissait le rituel. On s’assurait qu’il n’était pas un fantôme.

C’est ainsi que le gouvernement appelait une mince frange de la population, un peu plus large chaque jour, qui n’apparaissait plus sur aucune photo ni vidéo de surveillance. Bien que parfaitement visible pour leur concitoyens, plus aucune caméra ou capteur ne semblaient percevoir et enregistrer leur présence. Profitaient-ils d’un bug informatique, étaient-ils parvenus à pirater l’Algorithme ou bien était-ce là une forme de magie ? Nul ne savait. Mais on savait en revanche que ces fantômes étaient un grain de sable dans la machine. Un danger qui menaçait de faire sombrer la société, comme l’avait martelé le ministre au micro de tous les journalistes.

Alors, on avait ouvert une grande chasse aux fantômes, une guerre même. Une guerre médiatique tout d’abord, en quelques semaines, l’opinion publique les considérait comme de dangereux terroristes. Alors, on pouvait leur mener une guerre bien plus physique et tout ceux qui avait disparus des écrans pouvaient disparaître pour de bon. 

Par représailles autant que par conviction, les fantômes s’activaient la nuit. Profitant de leurs quasi-anonymat, ils s’évertuaient à briser, brûler, taguer, hacker ou faire exploser caméra, drones et capteurs sans qu’on puisse enregistrer leurs visages. 
C’était pour lutter contre eux qu’on avait instauré un couvre-feu. Quiconque était pris dehors après vingt-deux heures était susceptible d’être un fantôme.
Cette mesure servait bien sûr à protéger les honnêtes citoyens des balles perdues dans les affrontements avec les fantômes.

Presque déçu, le garde lui annonça que tout était bon, lui aussi aurait aimé jouir du prestige que représentait la capture d’un fantôme. 

— Vous n’avez appelé personne, fait aucune recherche, consulté aucun service hier soir ? Demanda le garde avant de le laisser partir. La question était certes intrusive, mais ne choquait plus personne. On n’avait plus rien à cacher, au contraire même, l’air du temps était à se montrer toujours plus.

— Non, j’étais malade, une sale migraine, passé ma soirée à dormir, mentit-il.

Il se nota mentalement que son obsession à se couper du monde devenait trop suspecte. Pour sa sécurité, il devait remplir son historique.

— Oh, grogna le garde — Sans y paraître, il fouillait les données médicales de Cédric. Il continua — Et vous n’aviez pas votre bracelet cette nuit, vous auriez dû, c’est important de suivra sa santé quand on est malade.

Lui n’avait surtout pas envie de porter son mouchard quand il était malade. S’il devait crever, il voulait en être le premier avertis et ne tenait pas à l’apprendre par une bardée de capteurs embarqués. 

— J’y penserai, fit-il mielleux, il avait hâte que tout s’arrête.

L’homme passa en revue le rapport de santé de sa montre. Heureusement, ses mauvais choix de vie, son anxiété permanente et son stress momentané lui offrait un taux de cortisol qui trompa le garde sur son état de santé. Enfin, il coupa l’analyse sur ses lunettes et s’approcha de Cédric pour lui souffler à voix basse. 

— Je seriez-vous, je le garderais toujours sur moi, il peut vous servir d’alibi.

L’échange prit fin en laissant Cédric troublé. Il avait l’habitude de cette injonction à ne jamais sortir de la surveillance, pour sa propre sécurité. On livrait toute sa vie pour prouver à la justice qu’on était innocent, le secret était devenu synonyme de culpabilité, c’était bien plus simple comme cela.  Mais il s’interrogeait sur le colosse, sa phrase était-elle une menace à peine voilée ou un conseil prévenant. Même chien de garde, il restait humain, avec ses failles, ses doutes, ses convictions. Cédric n’arrivait pas à croire qu’on puisse oppresser ainsi sans aucune raison, qu’on le fasse complètement et sans aucune once de réserve. C’était pour cela même qu’on employait de plus en plus de robots et de drones.

Il oublia cela en entrant dans la méga-surface. Il y faisait froid, les robot-vigiles y étaient effrayants et l’agencement y été étudiée pour rendre la circulation mal-aisée. En bref, on faisait tout pour encourager la population à faire leurs courses à distances et à se faire livrer. Cédric, par un esprit de contradiction qu’il n’expliquait pas bien, tenait à faire partie des derniers irréductibles, même si le combat était perdu d’avance. 
Ses repas, essentiellement des pesticides et des antibiotiques emballés dans du plastique, empilés dans son sac dos, il fit le chemin inverse. 

Il repassa donc devant l’écran géant, lequel l’informa de son inscription à un tirage au sort pour gagner un an de courses dans sa méga-surface. Puis deux publicités apparurent, une pour une application de rencontre en R.V et un spot pour un laboratoire vendant gélules contre les migraines.

On n’avait pas mis longtemps.

Un autre message, plus inhabituelle, tomba sur sa montre : 

\* seulement sur avis médical

L’algo avait-il compris qu’il avait menti, tenait-on à lui faire comprendre qu’on le tenait à l’œil ? Sombrait-il dans la paranoïa, ce n’était qu’un message publicitaire. Devenait-il cinglé ? 

N’était-ce vraiment qu’un message publicitaire ?

Il trancha dans ses réflexions. 

— Allez-vous faire foutre !

Sans faire attention à une quelconque écoute, oubliant la présence de tout espion potentiel, il répéta une seconde fois, détachant chaque syllabe.

Il rentra chez lui en ruminant et bouillonnant intérieurement. Plus rien n’existait autour de lui, il n’y avait plus qu’un orage mental qui arrachait tout sur son passage. Ses pas étaient si lourds que ses basket claquaient sur le bitume, il était le seul à ne pas l’entendre.

Il entra dans son immeuble, la seule présence de son bracelet électronique suffisait au système domotique à lui ouvrir la porte. 
Par mainte efforts, il réussit à calmer la tempête en lui, même si son esprit était encore agité.
Parfois, les ressacs de sa colère revenaient sans crier gare, lui redonnant envie d’écraser tous les mouchards disposés dans son modeste T.2. 
Même s’il faisait tout pour éviter les appareils bardés de micro et de capteurs chez lui, on l’avait obligé a en installer certains. 
Désormais, éclairage et chauffage « Intelligent » était obligatoire pour respecter la loi « Économie d’énergie ». Loi qu’on avait voté pour compenser la consommation faramineuse des systèmes urbains et des stockages de données des multi-nationales.

Il lança un regard torve à son assistant numérique, petit bijoux de technologie, posé sur sa table basse. Il était le cerveau de tous les autres gadgets présents dans son appartement, son espion personnel de poche. S’il n’était pas officiellement obligatoire, il était indispensable, sans lui, aucun accès au réseau n’étaient autorisés. Il était ni possible d’avoir de messagerie ou d’appel en vision-conférence, ni d’accéder aux services de l’administration, aux soins ou aux banques. Sans lui, la vie dans la société était impossible. Sans lui, on rejoignait le camps des marginaux, des reclus, des rejetés, des S.D.F. C’était tout ce qui retenait Cédric, même dans ses coups de colère de plus en plus fréquent, de l’éclater d’un coup de talon. 

Il se savait condamné à vivre dans cet entre-deux désagréable, fliqué en permanence et s’imposant une vie austère pour ne pas donner encore plus. Il avait supprimé tous les comptes qui ne lui étaient pas  indispensable, le coupant de tout divertissement numériques. Il était parvenu à court-circuiter l’accès au réseau de sa plaque de cuisine, mais se passait de four, de micro-onde, de bouilloire, tous les modèles disponible étant désormais connectés.

Parfois, il se surprenait à vouloir abandonner. À quoi bon, on le surveillait déjà de toute façon, alors un peu de plus ? Dans le troupeau, on avait l’air heureux après-tout. Au fond de lui, il savait tout de même qu’ils n’avaient que l’air. Mais il savait aussi qu’il ne pourrai tenir éternellement ainsi. À un moment ou à un autre, il devra se résigner. 

A moins que …

Depuis le matin, une idée l’obsédait, cette idée avait prit la forme d’un morceau d’emballage en carton. Durant la nuit, un fantôme avait profité de son invisibilité aux yeux des caméras pour s’introduire dans l’immeuble et glisser un message sous sa porte.

« Si tu veux nous rejoindre, laisse traîner un morceau de carton devant cette porte demain soir et laisse ouvert »

Le papier, hors fin publicitaire, était devenu rare et on avait fait avec les moyens du bord. Le message été écrit d’une main maladroite, de celle qui n’avait plus tenu un stylo depuis vingt ans. L’encre était le seul moyen de communication sur lequel on ne pouvait pas encore installer de mouchard. 
Il reconnaissait bien là l’esprit de tribu des libertaires qu’il avait fantasmé. Ils étaient prêts à inviter d’illustres inconnus dans leurs rangs. Était-ce là de la tolérance ou une naïveté suicidaire ? Il trouva tout cela fort simple. Comment un clan aussi facile à intégrer n’était t-il pas encore infiltré par un bataillon de flics ? 
Où alors était-ce là un piège, une procédure d’enquête pour tester sa loyauté ? S’il acceptait finirait-il menottes aux poignets ?

Mais si c’était vrai ? Et s’il pouvait enfin s’échapper, être un de ces électrons libres qu’il avait toujours rêvé d’êtres ? Il n’aurait sans doute qu’une seule chance. 

Comme souvent quand il était en plein doute, oscillant entre la résignation et la révolte, il s’installa à son bureau. Il s’était aménagé un coin de vie privée, quelques mètres carré seulement hors de vue de la moindre caméra. Il retira son bracelet, qu’il lança négligemment sur son lit et sortit un vieux cahier et son stylo. Les deux artefacts dataient d’un temps où on les surproduisaient et sur surconsommaient encore. Le temps du dernier baroud d’honneur de l’encre et du papier. 
Aujourd’hui, ces objets étaient quasiment introuvables et étaient probablement ce que Cédric possédait de plus cher. 
Il s’était construit un rituel quasi-religieux, il s’installait le dos courbé, le nez proche du papier, puis il traçait ses mots, d’une écriture tranchante et anguleuse. 
Il consignait tout, ses rêves, ses fantasmes, ses déceptions, son mal-être et ses joies, ses peurs, ses dégoûts et ses colères. En noircissant son cahier, il pleurait, grognait et exultait silencieusement, à l’abri du regard du monde. Dans le secret de sa conscience.
Quand sa nuque, ses phalanges et son poignet étaient devenus douloureux, la lumière de son appartement s’était adapté automatiquement à la nuit tombante. 

Il avait prit sa décision.

Pour la dernière fois, il remplit son historique de mauvais articles de presses, de sites comparatifs de gadgets voué à l’obsolescence et de vidéos abrutissantes.

Il déposa un morceau de carton devant sa porte et attendit. Par sécurité, il alla chercher un couteau de cuisine qu’il posa devant lui. Il ne comptait pas se défendre avec cette lame si dérisoire. Il se préparait à s’ouvrir la gorge s’il voyait un flic. Il ne se surprenait plus à envisager sa mort aussi froidement, il avait déjà  plus d’une fois envisagé de verser son sang. 
Mais ce temps était révolu, Du moins il l’espérait, c’était sa dernière issue, la dernière avant la grande porte.

Pour passer le temps et pour ne plus prêter attention aux coups de feu qui crevaient la nuit, il se mit la dernière série à la mode. Tout cela ne lui manquerait pas. 

Enfin, quand il entendit sa porte s’ouvrir, il se jeta sur le couteau et se tourna vers le visiteur.

Elle ne pouvait être qu’un fantôme, il fallait être invisible aux caméras pour oser porter ce genre d’accoutrement dans un monde aussi policé. Une femme aux cheveux tirés en chignon se tenait devant lui. Elle portait une veste de travail bleue sur laquelle un gilet de pêche était jeté. Un jean déchiré et des chaussures de randonné défoncées terminaient le tableau.  Contrairement à ce qu’il avait imaginé, bercé par les mythes punks du début du siècle, la fantôme n’arborait ni tatouage ni coupe de cheveux extravagantes. Il comprit bien plus tard que tout cela était un moyen d’identification bien pratique qui ne convenait pas aux clandestins.

L’invitée esquissa un geste de main apaisant devant l’arme et expliqua avant qu’il ne puisse dire un mot. 

— Ne dis rien, fais toi comprendre par geste, je ne suis pas enregistré par tes micros, mais si tu te mets à parler tout seul, tu vas attirer l’attention.

Cédric acquiesça, puis, pour en avoir le cœur net, afficha sur son écran la vue de sa caméra de sécurité. Il apparaissait seul sur l’image.

— Bon réflexe, approuva la fantôme, t’as un endroit de libre ?

Une nouvelle fois il acquiesça silencieusement et la guida vers son bureau. 

— T’es bien sûr de ce que tu fais ? Pas de retour possible après. Au moindre contrôle de jour, t’es mort. Plus d’appart officiel, plus de boulot, plus de magasin, tu ne fais plus partie du monde !

Il ouvrit son carnet et écrit : 

« Vas-y »

Son cœur rata un battement, son monde tourna quelques instants. Une digue sembla se briser en lui, son existence en était retournée mais son torse se libéra d’un poids. 

— Je t’explique. Je vais te piquer, une sorte de vaccin contre la connerie, si tu veux. Après ça, plus aucun enregistrement numérique ne pourra te capter, tu agis comme une sorte de mini-brouilleur. Ça marche aussi avec l’infrarouge et les détecteurs de mouvement. Fais gaffe aux aberrations que tu provoques. Si tu touches un objet, il disparaît aussi des enregistrements. Et il ne faudra pas longtemps pour qu’un algo préviennent le système si un mouchards détecte ce genre de bug. En clair tu vas galérer à crécher et a bouffer, ça te va toujours ?

C’était de toute façon trop tard pour reculer. 

— Alors, c’est parti !

Il tendit son bras, elle sortit une seringue de son sac à dos. Les protocoles sanitaires et sécuritaires étaient bien plus lâches chez les clandestins que chez les assermentés.

Une douleur fugace et il venait de mourir, au moins pour le reste du monde. De son point de vue, il venait de renaître. Il essuya une goutte de sang qui perlait pendant qu’elle lui donna les dernières explications. 

— Dans quatre heures, t’es invisible, d’ici là, pense à ta façon de te barrer et de te planquer. Si tu veux nous rejoindre, je peux en causer aux autres, ça devrait le faire. Je t’oblige à rien, hein, si tu veux rejoindre un autre groupe, ou rester seul, c’est ton problème.
Il écrivit sa réponse sur son carnet. 

« J’y penserai, mais avant j’ai à faire »

— Dis, l’ami, t’as une belle écriture, tu peux me faire une copie des messages, ça fera moins plouc que si c’est moi qui écris ?

Reconnaissant, il accepta et sous la dictée de la jeune femme, il écrivit une série de messages, certains identiques à celui qu’il avait reçu, d’autres plus énigmatiques protégé par un code.  Il déchira les pages de son carnet et lui tendit. 

— Merci pour ça, l’ami ! Dit-elle en pliant le papier et en le rangeant dans une poche intérieure de sa veste. Bien sûr pas un mot si tu te fais chopper !

Il acquiesça. 

— Je traîne pas plus longtemps, on se reverra peut-être plus tard, un type comme toi peut être utile. Aller, bye !

Elle partit sans plus de cérémonie. 

Cédric ne réalisait pas encore. Tout cela avait été trop simple, trop rapide. Loin de la longue initiation que son esprit romantique avait imaginé. L’idée que sa vie puisse être aussi aisément bouleversée le terrifia autant qu’elle le soulagea. Lui, au moins, avait eut le luxe de l’accepter. Une fois de plus il se sentait privilégié. Maintenant, et comme peu d’autre, il avait en main son avenir. Il savait que la moindre erreur pouvait désormais lui coûter une balle dans le cœur. Le bruit d’une lointaine fusillade lui donna raison. Il vivait sur la corde raide, donc il vivait. 

Il alla s’allonger sur son lit pour la dernière fois. Il ne dormait pas, il attendait. Son esprit oscillait entre les phases d’euphories et d’angoisses profondes. Il rêvait de ce qu’il pouvait et ne pouvait plus faire. Puis quand vient l’heure, il effectua le dernier contrôle. Il afficha l’image d’une de ses caméras. Il n’apparaissait plus. Il était invisible, il était devenu un fantôme.

En vitesse, il prépara un grand sac à dos, prenant bien soin de laisser sur place tout ce qui était susceptible de contenir un mouchard. Un manteau, quelques jours de provision et une gourde. C’était un bien faible équipement pour partir sur les routes. 
Il fut tenté d’en finir définitivement avec son ancienne vie en pulvérisant son bracelet. Il se retient, il lui fallait repousser au plus loin le moment où on se rendrait compte de sa disparition. 

Puis enfin il sortit. Le couvre-feu prenait fin d’ici deux heures, d’ici là, on pouvait le descendre sans sommation. Après ça, chaque contrôle l’enverrait à coup sûr au cachot. Il tenta d’ignorer la boule qui grossissant dans son ventre. 
Il marchait d’un air pressé, les yeux aux aguets, sursautant aux moindre bruits, à la moindre ombre passant dans son dos. Dix fois il crut mourir quand son cœur rata un battement.

Puis petit à petit à petit, il s’habituait, il marchait avec une plus grande confiance. Puis, il goutta au sentiment grisant de passer dans une rue sans déclencher les lampadaires à détecteur de présence. L’illégalité lui donnait des ailes, il se voyait comme les pirates d’antan dont il avait tant rêvé.

Alors que le soleil gagnait contre la nuit, l’ordre gagnait contre les fantômes qui devaient se disperser, les panaches de fumées au loin et les coups de feu se firent de plus en plus rares. 

Cédric, oubliant un peu de prudence, força le pas, il devait être sortit du centre-ville et ses patrouilles avant la reprise de l’activité régulière. Il savait aussi qu’il n’avait qu’une heure avant que son manager déclare son retard à la pointeuse et officialise sa disparition. Il savait que l’Algo allait fouiller son passé pour le retrouver, mais il doutait qu’on envoie des chiens de gardes enquêter personnellement sur lui, le ministère comptait sur une erreur des fantômes pour leur tomber dessus. 
Mais lui ne ferai pas d’erreur, il savait qu’il devait se cacher, des yeux humains et non plus de ceux des caméras. Et les yeux de chaire sont bien plus faciles à tromper que ceux de verre et de silice.

Pendant les quatre jours que lui permettaient ses provisions, il adopta le mode de vie des renégats. Il marchait de nuit, évitant du mieux qu’il pouvait les patrouilles, mais saluant poliment d’un signe de main les drones qui le survolaient. À l’aube venu, il cherchait une faille humaine pour accéder là où un bracelet aurait dû lui accorder l’accès. Il squatta dans son errance cave, local à poubelle et recoin de dépôt de stockage, là où les caméras tournaient en permanence, mais où les citoyens passaient peu où en vitesse. Durant ses quatre jours il connut les courbatures de la marche là où il ne connaissait que celle des salles de sport, qu’il ne fréquentait plus depuis longtemps. Il connut l’adrénaline de chercher une cachette et la jubilation d’enfin la trouver, là où il ne vivait que dans la morne routine. Il appris a rationner l’eau et la nourriture là où un simple ordre vocal lui permettait de commander et se faire livrer n’importe quoi. Mais surtout il connut la douceur d’être en sécurité là où beaucoup d’autres étaient broyés.


Enfin, après avoir parcouru la moitié d’un secteur et passé à la nage un canal pour éviter un point de contrôle, il arriva dans un petit quartier pavillonnaire.

Même si la fin du couvre-feu était déjà passée, Cédric savait qu’il n’avait qu’à craindre les équipes de soins à domiciles qui rodaient dans ce quartier à la population vieillissante. Pour ne pas faire disparaître un portail aux yeux des caméras, il passa au travers d’une haie de thuyas et se retrouva dans le jardin de ses grand-parents. Un mélange indistinct de nostalgie, de soulagement, d’appréhension et d’impatience infusa en lui. 
Enfin, il aperçu sa grand-mère, assise dans un fauteuil de jardin installé dans sa pelouse. 

Comme l’avait fait la fantôme en se présentant à lui, Cédric, la salua et lui demanda de ne pas parler.  Avec un petit sourire, elle termina de résoudre son casse-tête en Réalité Virtuelle, puis invita son petit fils à la suivre dans à l’arrière du jardin, là où elle entretenait ses rosiers. 

— Bonjour mon grand, commença, t-elle, tu es enfin devenu un fantôme ? Ici, nous somme tranquille, j’ai désactivé mon système d’alerte senior et tous les voisins sont sourds comme un pot !

— Oui, les fantômes m’ont trouvé et ...
— Ils ont eu un peu d’aide, coupa la grand-mère, un sourire malicieux aux lèvres.

D’un accord silencieux, ils décidèrent de ne plus évoquer cette question.

La grand-mère de Cédric n’avait pas changé avec les années. Elle portait toujours ses polaires aux couleurs pastelles et ses bijoux en corde et perles de bois. Elle avait toujours cette façon de ne parler qu’à demi mots et de ne pas pointer les évidences. Mais hélas, le temps passait et son visage s’était ridé, ses cheveux avaient blanchis, sa posture s’était courbé et sa voix commençait à légèrement trembler. Cédric voyait sa joie de la revoir assombrit par la culpabilité. À défaut de savoir quoi faire ou dire, il fit la conversation.    

— Je vois que tu as agrandis ta collection, reprit-il en désignant les dizaines d’espèces de fleures.

— Et oui, depuis le temps, j’ai trouvé une passion qui m’occupe et qui n’attire pas trop l’attention.

— Je suis désolé, de ne pas être venu plus souvent, surtout depuis …

Elle le coupa, 

— Ne t’inquiète pas pour ça, le principal c’est que tu sois là et j’espère que tu vas rester quelque-temps.

Cédric n’avait pas encore osé lui demander, mais il se devait d’être prudent. 

— Je ne voudrais pas te poser de problèmes.

— Ne t’inquiète pas pour ça, garçon, il y a longtemps que j’ai pris mes dispositions. Et je crois que mon système ne fonctionne pas trop mal.

Le petit fils, même s’il ne se faisait pas d’illusion, espérait que sa grand-mère avait arrêté. 

— Ne prend pas trop de risques, avec ton passé et celui de Maman et Tonton, ils doivent t’avoir à l’œil.

— Ne t’en fais pas pour moi, j’ai passé des années loin de tout ça, histoire de me faire oublier. J’ai un compte sur tous les réseaux et je me suis fait un joli petit score de réputation. J’ai même accepté une surveillance médicale, c’est dire ! Je suis la mamie gâteau parfaite ! 

Elle réussissait ce qu’il n’avait jamais voulu faire, intégrer le jeu pour mieux le contourner. Il se sentit presque mal en comprenant que sa fierté à ne pas se compromettre dans la surveillance était d’une inefficacité certaine et d’un égoïsme crasse.

— Mais ça a été plus fort que moi, quand ils ont commencé à nous surveiller même dans nos chambres, j’ai repris. Ce n’est pas parce que ma génération à perdu la bataille, qu’on ne peut pas vous aider à réparer nos bêtises.

Maintenant qu’il était un fantôme, Cédric prit la franche résolution de continuer la lutte de sa famille, il voulait que sa grand-même puisse enfin prendre une retraite sereine. 

— Crois moi que si je n’avais pas passé la date de péremption, j’aurais aimé devenir fantôme, mais je n’en suis pas un, alors je dois vite revenir en vue des caméras, sinon mon système d’alerte va s’activer et les pompiers vont débarouler. Va à la cave, il y a les affaires des enfants, tu peux te cacher là.

La grand-mère entra dans la maison et laissa la porte grande ouverte pour que Cédric n’ait pas à la toucher. Il connaissait par cœur l’agencement des pièces, mais tout lui semblait plus petit que dans ces souvenirs. L’odeur, elle au moins, n’avait pas changé.

Il entra dans la cave et une partie de lui-même, avec laquelle il c’était longtemps coupé, lui revenait en pleine figure. Le choc lui fit perler une goutte de sueur. La grand-mère avait, dans ce sanctuaire coupé de la vigilance du monde, entreposé en vrac les veilles affaires de ses enfants. On y trouvait du matériel informatique datant d’au moins quarante ans, des disques durs remplis d’années de vie, du matériel de dessins ayant bien trop vieillis, des peintures et des esquisses aux couleurs passées et de vieux instruments de musiques désaccordés. Tout cela datait d’un temps où l’on croyait encore qu’encourager la jeunesse à se jeter dans la création les aiderait à supporter la charge de travail qu’on voulait leur imposer. Désormais, on savait que pousser les gens vers l’art, c’était les pousser à interroger le monde. 

Le reste de la salle servait de lieu de rendez-vous à d’autres marginaux, une petite table ronde était installée à côté de deux matelas gonflables. Plus loin, de grandes caisses en plastique attirèrent l’attention de Cédric. Elles étaient remplies de vieux romans, bandes dessinées et revues. Il en reconnus quelques-unes qu’il avait lu enfant et qui déjà à l’époque étaient des antiquités.  Mais la collection avait grossit, il soupçonna sa grand-mère d’avoir caché ici tous les livres qu’elle avait pu sauver du recyclage, sort qui les menaçait depuis la grande fièvre de numérisation des vingts dernières années.

Alors, Cédric se perdit toute la journée, retrouvant les héros avec lesquelles la génération de ses grand-parents avait grandis. Si les plus connus de l’époque existaient encore un peu dans la culture populaire, souvent sous forme de clin d’œil « Vintage » dans la publicité, beaucoup d’entre eux avait été oubliés. Et en lisant les aventures des héros gaffeurs des B.D pour enfant, les extraordinaires épopées des romans de fantasy ou de science-fiction, les sombres histoires des justiciers aux passés tortueux des comics, Cédric comprit la rage obstinée de sa grand-mère à vouloir les protéger. Mais il comprit aussi qu’elle n’était pas une combattante jusqu’au-boutiste, prête à incendier des caméras de surveillance ou à tirer sur des drones. C’était une rêveuse. Comme lui. 

Alors, il savait quoi faire, des le lendemain, avec l’aide de Marion, la jeune fantôme qui l’avait piquée et qui passait régulièrement chez la grand-mère, il se mit au travail. 

Dès lors, une ou deux fois par semaines, le grand panneau publicitaire du centre-ville était recouvert d’affiches, de reproduction de planches de bandes dessinées, de passage de roman, de partition, de caricatures qui faisaient la grandeur de la presse d’autrefois. Quand on sécurisa bien trop le sacro-saint panneau. On se mit à les placarder sur les murs, sur les carcasses de robots, on les faisait passer sous les portes des appartements. Bientôt d’autres rejoignirent le mouvement. On ressortit d’autres anciennetés cachées ailleurs, les ateliers de reproduction se multiplièrent dans les greniers et les sous-sols. Les derniers hackers, qui n’avait pas été attrapé ou passé de l’autre côté, se mirent en tête d’afficher directement les cases de B.D sur les panneaux publicitaires, d’envoyer des poèmes ou des citations directement sur les bracelets connectées des honnêtes gens. Six mois plus tard, l’incendie avait prit et s’emballait. On les imitait dans d’autres villes, on mettait en place des espaces de téléchargements des œuvres numérisées, quand un était fermé par les forces de l’ordre, deux autres apparaissaient. Bientôt le corpus s’enrichit, des fantômes se découvrirent des dons d’écrivain, de scénariste, de poète ou de dessinateur. On commença à s’échanger sous le manteau des feuillets et même l’Algo ne put contrôler ces échanges. Au bout d’un an, les citoyens les plus courageux n’hésitaient plus à perdre des points de réputation pour partager à leurs cercles les textes et dessins qui les avaient touchés. Ce monde, qui en avait bien besoin, commença à ce peupler de rêveurs.                    

  