---
title: Le Sabbat
date: Octobre 2016
ordre: 5
summary: Les feuilles d'automne crissaient dans leur cercueil de givre sous les pieds nus de Mélia "Oeil-de-crapaud". D'humeur encore plus grincheuse qu'à l'accoutumée, la guenaude pestait à pleine voix...
---

Les feuilles d'automne crissaient dans leur cercueil de givre sous les pieds nus de Mélia "Oeil-de-crapaud". D'humeur encore plus grincheuse qu'à l'accoutumée, la guenaude pestait à pleine voix.

La sorcière sauvage ne supportait personne et réciproquement, aussi, elle ne quittait que rarement ses marais puants. Quand cela devait arriver, elle devenait plus détestable encore.

Sa voix grinçante et nasillarde fit même fuir les corbeaux qui s'envolèrent des branches décharnées de la forêt des pendus.

Œil-de-crapaud maudissait le jour où elle avait rejoint le Conseil des Jeteuses de Sorts Pernicieuses et Ensorceleuses Peu Recommandables. Elle était depuis obligée de se rendre de façon biannuelle au sabbat organisé par la pas si noble assemblée.

Le sabbat d'Halloween était encore pire pour elle. Non pas que l'assemblée de février l'amusait, mais la fin du mois d'octobre était la pleine saison des champignons vénéneux. Chaque année, elle devait quitter sa parcelle pour la réunion, découvrant à son retour sa récolte dévorée par les chenilles toxiques.

— Forcément, quand on fait du plan de mandragore en quantité industrielle, on ne pense pas aux petits artisans !  Je t'en mettrais moi du "Personne ne sera oublié" ! Vociféra Mélia comme pour se donner du courage sur le chemin traître du bosquet aux vipères.

— Trois jours aller, trois jours retour, sur des sentiers pourris. Tout ça pour faire les guignols dans une réunion où il ne se dit rien, se fait encore moins !

De rage, elle donna un coup de pied dans un caillou. Celui-ci eut la politesse de lui rappeler que le sol local était plus dur que la terre boueuse et inondée des marais. Elle jura bruyamment, maudissant son terrible agresseur avant de reprendre sa charge.

— Et vas-y que ça glousse et vas-y que ça cancane et vas-y que ça brasse du vent ! En plus, on se pèle dans ce patelin paumé !

Depuis la nuit des temps, une rivalité entre les sorcières sauvages, vivant en ermites dans les coins les plus inhospitaliers de la contrée et les citadines vivant prêt des mortels continuait. Sous la direction des enfers, les deux factions s'étaient rapprochées au court des siècles, mais personne n'en avait vraiment envie. Et puis dans une communauté aussi aigrie et désagréable qu'étaient les lanceurs de sorts, les rancunes étaient tenaces.

— Vraiment, je ne sais pas ce qui me retient de leur envoyer un vieux rat crevé comme réponse à leur invitation la prochaine fois. Le message devrait être assez clair : je me casse bande de ploucs, on se revoit en enfer !

Il est intéressant de noter qu'à ce moment précis, Œil-de-crapaud ne râlait que pour se maintenir dans l'ambiance. Elle savait très bien ce qui la forçait à rester. Le modeste présent qu'elle voulait envoyer provoquerait, elle connaissait le savoir vivre de ses consœurs, une envie de renvoyer une offrande en retour. Ce genre d'escalade ne pouvait que mal se terminer, pas seulement pour la faune locale, que l'on avait pourtant tendance à clouer sur les portes des maisons en guise de courrier amical.

Et puis il y avait autre chose. Une raison bien plus mystique que cette prosaïque histoire de communication traditionnelle...

Quand, prise par la relative naïveté de la jeunesse, il y a soixante-douze ans et quatre mois, elle avait signé pour intégrer le CJPEPR, elle avait fait un pacte de sang auprès du Malin. Elle n'avait plus en tête les clauses du dit contrat, mais il ne lui semblait pas que leur Seigneur à tous avait une habitude pour les ruptures de contrats à l'amiable.

Trop occupée à râler, elle oublia de faire attention où elle posait ses pieds nus, sa cheville s'enfonça dans un nid de ronces. La douleur la rappela à l'ordre.

— Saleté, quand on invite du monde, la moindre des choses c'est de nettoyer chez soi !

Elle extirpa sa jambe de l'emprise de la plante en grimaçant quand les crocs végétaux mordirent la corne de son pied. Libérée du piège, elle fit un pas en marmonnant dans la langue des anciens mages. Ce pied de ronce n'allait pas s'en sortir indemne. Elle allait d'abord le tuer par le givre. Une fois mort, elle transformera la tige en pierre, qu'elle fera tomber en poussière qu'elle dispersera d'un coup de talon rageur.

C'était puéril, mais la rage qu'elle avait fait monter en elle depuis son départ devait s'exprimer.

Elle se ravisa à la dernière seconde.

Elle était déjà sur le territoire de son hôte, Griibeen la démone, il était donc de bon goût d'éviter autant que possible de heurter le territorialisme de la chef des sorcières du pays. Mélia ne tenait pas à se retrouver dans une bonne vieille guerre de territoires. En bonne meneuse de troupe, Griibeen devait être irréprochable, elle avait donc beaucoup de défauts, laide, méchante, sournoise, égoïste ... Mais on ne pouvait lui enlever son sens de l'humour acéré.

Non, définitivement c'était un coup à voir son marais si fertile transformé en marais salant.

***

Griibeen organisait chez elle, dans la forêt des pendus, le sabbat depuis trente-huit ans, temps depuis lequel elle s'était auto-proclamée directrice du CJPEPR. Le conseil n'avait reçu aucune directive de la hiérarchie quand à la direction du concile, mais quiconque contestait l'autorité de la Démone était cible d'une de ses farces maison. Tradition folklorique à base de transformation en têtard jeté en pâture à son chat borgne.

Se souvenant qu'elle se rapprochait enfin du conclave, elle mit sa mauvaise humeur en sourdine, elle ne tenait pas spécialement à ce que Griibeen sache ce qu'elle pensait du sabbat. Elle était déjà en retard, pas la peine de se faire remarquer d'avantage.

Néanmoins, le silence de mort dans la forêt attira l'attention de la sorcière. Elle le prit tout d'abord comme une bonne nouvelle.

— Zinzra l'hystérique n'est pas là, on ne l'entend pas hurler à des milles à la ronde. Peut-être que le chevalier qui en avait après elle a réussi à l'avoir finalement, le diable soit loué.

Mais quelque-chose clochait vraiment, pas de musique, pas d'éclat de voix, pas de pleurs d'enfants terrorisés.

Elle pressa le pas, l'adrénaline monta en elle.

— Si ça a changé d'endroit, je vais rater la fête et je suis bonne pour être rappelée pour rôtir au bercail !

Elle fonça à travers les châtaigniers agonisants. Arrivée au point de rendez-vous, la surprise manqua de lui faire sauter les yeux des orbites.

— Griibeen, tu ... que ... ?

La Démone était là, seule, son visage tordu était morne, ses yeux d'un rouge délavé perdus dans le vague. Assise sur une des treize pierres disposées en cercle pour accueillir les représentants les moins fréquentables de la population. Elle démembrait sans conviction le squelette d'un corbeau, décoration installée pour l'occasion. Un long soupir plaintif lui échappait à chaque fois qu'elle balançait négligemment un osselet arraché au squelette.

Elle lança un faible sourire à Mélia et lâcha.

— Bonsoir, Œil-de-crapaud !

La moutarde montait gentiment au nez du dit Œil-de-crapaud.

— Quoi, tu m'as faite venir de l'autre bout de la contrée pour rien, t'as intérêt à me ... 

Tous les voyants dans l'esprit embrumé par la colère de la sorcière passèrent au rouge, elle ravala sa salive. À parler sans réfléchir, elle était mûre pour la fable du chat et du têtard.

Elle ferma les yeux ...

Rien ...

Peut-être que la transformation n'était pas si douloureuse.

Elle rouvrit les yeux et constata que sa peau n'était pas devenue plus visqueuse qu'à l'accoutumée.

La démone essuya une larme et continua,

— Je te déteste, mais je suis contente que tu ne m'aies pas trahie

Interdite, Mélia, s'approcha et s'assit sur une pierre proche de sa cheffe.

— Tu vas m'expliquer ?

— Quand je n'ai vu personne arriver, j'ai éventré un furet pour y lire ce qu'il se passe. C'est Ysmii, elle a organisé un contre événement.

— C'est sympa de voir qu'on ne m'invite pas ! Pesta Mélia, par réflexe, la compassion n'avait jamais été dans ses habitudes de toute façon.

— Si, il y avait une invitation qui est partie par chat voyageur pour les marais, je crois, je te remercie de ta loyauté.

— Ah, mon piège à hérisson est peut-être mal calibré alors. En même temps, c'est fou le poids qu'ils prennent ces ...

Elle s'arrêta brusquement, s'apercevant que sa stratégie n'était peut-être pas la plus adaptée à la situation.

Une fois encore, la réaction fut faiblarde.

— En fait, c'est toi qui as raison, vivre de pièges et de cueillette sans te soucier de la politique, connerie de mondialisation, je te dis !

Mélia vit venir l'embuscade, Griibeen était à deux doigts de lui demander de l'emmener avec elle dans les marais, pour y vivre quelques temps d'air lourd et d'eau croupie et soigner sa déprime saisonnière.

Griibeen, ne tenait pas à avoir d'invité dans ses terres et encore moins la démone. Il était de notoriété publique que les citrouilles du marais étaient chargées en fibres et minéraux, la cure de vitamines aura tôt fait de remettre sur pied la directrice du conseil. Une fois dans son assiette elle ne mettra pas longtemps pour coller le nez dans les affaires de Mélia. Elle voulait éviter que certaines ventes qu'elle assurait, sous le manteau, histoire de s'assurer une petite retraite peinard, ne se sache. Si les sorcières avaient tout appris de l'art des montages financiers des humains, ces derniers sont d'un triste manque d'imagination quand au sort qu'on réserve aux fraudeurs.

Elle changea donc rapidement de sujet.

— Toutes ces harpies ne se sont pas alliées avec les vampires au moins ? S'interrogea t-elle en exagérant une grimace de dégoût, astuce peu inspirée pour signifier un geste de compassion.

— Non, on a plus vampire dans les pics brumeux, ni de loup-garou d'ailleurs.

— Ils ont enfin décidé de retourner dans leurs montagnes et se sont faits enfermer à la fourrière ?

— Non, non, c'est vrai que t'es pas au courant, toi. On a lancé une rumeur en juin, que le chef des Crocs De Sang avait pour projet d'offrir en cadeau de mariage à la fille du chef des Yeux-d-enfer un manteau taillé dans la fourrure du mal alpha des Pisteurs du Nord. Ces abrutis se sont entre-tués tout l'été. On avait plus qu'à faire une descente pour se débarrasser des survivants.

— Joli coup, convenu Mélia admirative.

— Mais tout ça pourquoi ? Je te le demande ! Nos vies n'ont pas changé depuis. En fait ils étaient nos ennemis seulement car qu'on l'avait décidé. Alors, pourquoi tant de sang ...

Elle soupira et décrocha un squelette d'écureuil qu'elle commença à maltraiter également.

Un grincement s'échappa des dents noirâtres de Mélia. Elle avait vu Griibeen échafauder milles plan pour se débarrasser de la concurrence. Une victoire aussi nette aurait dû la rendre ivre de joie. La déprime de la démone n'était pas qu'une mauvaise passe, la trahison des sorcières avait profondément affecté son moral.

Mélia chassa de son esprit un fugace, "Tant de réunion de guerres au sabbat, pour ça" et repris.

— C'est vrai, mais c'était bien joué.

— Une idée d'Ysmii, cette fourbe de traîtresse.

— On reste dans le thème des sales coups, conclut Mélia avant un long silence.

Elles restèrent là, perdue dans leurs pensées durant un moment.

Finalement, Griibeen relança la discussion quand le calme devint insupportable.

— Que va-t-on faire maintenant Mélia ? Plus de conseil, plus de mission, plus l'envie.

"Toi ce que tu veux, moi je rentre chez moi, continuer mes petites affaires et je m'ouvre une bouteille de vin d'aconit, pour fêter ma paix retrouvée avec la fin de ton assemblée de givrées du chapeau." Vint instinctivement à Mélia, mais elle doutait de la pertinence du propos. Elle préféra opter pour une réponse destinée à redémarrer la volonté de la démone, plus adaptée à des considérations carriéristes.

— On va lancer les représailles les plus terribles que le diable ai connus. Je te jure que ça va saigner. Après ça, on est nommée direct lieutenants infernaux ! j'espère que ton chat a un grand appétit !

Elle était assez fière de son coup, elle allait provoquer une guerre en s'assurant d'être du bon côté. Finalement, la politique, ça a quelque chose de distrayant.

— Bof, tu sais, je suis fatiguée des guerres, des vendettas, de la violence et des têtards. Tout ça ne mène à rien. Et puis Faylyx en a marre aussi des batraciens, il ne les digère plus je pense. Lui aussi a droit à un mode de vie plus sain.

— Bah en souris, ou en lézard alors, un régime alimentaire varié, rien de mieux pour ces petites bêtes ! Renchérit elle en imaginant déjà toutes les sorcières qui l'ont moqué durant des années terrifiées devant le courroux de la démone.

— Non n'insiste pas, j'arrête ces conneries. Vas-y si tu veux, moi j'arrête les frais.

La perspective d'aller provoquer le nouveau Conseil, seule, étant d'un coup moins amusante, elle coupa court au débat.

— Mais si on ne fait rien, notre seigneur va prendre ça pour une trahison et on va se faire cramer.

Griibeen eut un petit sourire triste.

— Non, sois tranquille pour ça ... Le contrat c'est un faux, juste histoire de s'assurer que les sauvages comme toi ne trouve pas mieux à faire que venir, on sait que c'est mal foutu ce calendrier quand on vit de cueillette.

— Attends, tu me dis que ...

— Le Diable n'en a plus rien à foutre de nous, les mortels se débrouillent très bien pour foutre le bordel tout seul.

Mélia se leva, folle de rage, les poings et la mâchoire serrées, la respiration haletante et les tripes en feu. Depuis des années elle avait été dupée par cette sorcière, cette teigne qui se tenait devant elle a l'état de loque.

Elle voulait lui lancer ses pires malédictions, que ses yeux soient bouffés par les vers, que ses poumons se remplissent de bave de crapaud, que ses dents deviennent comme des briques et qu'elle s'étouffe avec sa langue transformée en serpent.

Elle étouffa un hurlement dément qui aurait glacé le sang à tous les enfants à des milles à la ronde. Foutre le feu à cette forêt maudite et à chaque parcelle de territoire sorcier qu'elle trouvera sur le chemin du retour.

Finit Œil-de-crapaud la solitaire, elle allait mettre la vie de cette bande d'escrocs à feu et à sang !

Et puis non ...

Sa fureur s'échappa dans un simple,

— Ah bon...

Elle se rassit. Dupée depuis des décennies, sa vision de son ancestrale profession venait de s'écrouler. Elle aussi avait besoin d'une sérieuse introspection. Griibeen, compréhensive, lui tendit un petit squelette qu'elle se mit à démembrer avec la même application.

Elles restèrent là une bonne partie de la nuit, le silence de cette sinistre soirée d'automne seulement ponctué par le sifflement d'un vent piquant et le craquement du cartilage.

Prise par une détermination soudaine, comme illuminée par la lumière de ses réflexions profondes, Mélia proposa un retournement d'activité pour le moins acrobatique.

— Le truc qui marche pas mal en ce moment, c'est le druidisme. Ça reste un peu mystique, on a nos chances non ?

— Je sais pas, à mon age, les études, ça ne me dit rien.

— Il y a le Meneur de loups dans la forêt éternelle, il pourrait nous faire une formation accélérée.

— Peut-être, c'est un ancien de la maison, il avait raison le bonhomme. Il s'est barré avant que ça ne parte à vaux-l'eau, le mal.

— Et puis le grand aire, la vie autour des sources, parler avec les lapins et la protection des oiseaux ça peut pas nous faire de mal ...

D'un mouvement de tête d'une rare coordination ils jetèrent un regard au tas d'ossement de divers petits animaux à leurs pieds.

— On a pas mal de travail quand même !

— De toute façon je suis allergique aux poils de clébards ...

On évoqua tout le reste la nuit les diverses branches magiques, des plus éculées au plus exotiques. Au petit matin, elles se rendirent à l'évidence, la reconversion n'était pas chose aisée.

— Et chez les mortels, il n'y a pas de filon, tu les connais un peu toi, non ? Proposa Mélia à court d'idées.

— Bof, la dernière fois ils voulaient me brûler

— Forcément, mais si on n'y va en paix ?

— Non, ça ne marchera pas, on est trop ...

Elle chercha quelques instants ses mots,

— Comment dire ... Moches ! au premier village ils nous lapideront.

— Moches ? S'interrogea Œil-de-crapaud, pour qui le miroir n'était pas une technologie connue.

— Oui, la peau grisâtre, le dos voûté, les ongles sales, les chapeaux ringards et les verrues, c'est pas trop à la mode chez eux.

— On a qu'à se débrouiller pour faire en sorte que ça le soit. Proposa-t-elle un peu naïvement.

— Ça me semble un peu compromis dit comme ça ... On fait pas plus vaniteux qu'eux ...

— Même une journée, comme un événement comme l'autre gros barbu rouge en décembre ?

— On peut essayer, mais à mon avis, ça va foirer ...

— Qu'est ce qu'on risque, au point où on en est ?

— De lancer une fête la fête des citrouilles, des masques moches, des vieux balais et des bonbons ...

***

Maintenant, vous repenserez au chat et au têtard quand on vous hurlera dessus pour la cinquième fois de la journée

« Des bonbons ou un sort ! »